<?php
$bunch_sc = array();

//Meet & Ask
$bunch_sc['bunch_meet_and_ask'] = array(
			"name" => __("Meet & Ask", BUNCH_NAME),
			"base" => "bunch_meet_and_ask",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Meet & Ask.', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Title', BUNCH_NAME ),
						   "param_name" => "btn_title",
						   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Link', BUNCH_NAME ),
						   "param_name" => "btn_link",
						   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Title', BUNCH_NAME ),
						   "param_name" => "btn_title1",
						   "description" => __('Enter The View More Button to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Link', BUNCH_NAME ),
						   "param_name" => "btn_link1",
						   "description" => __('Enter The View More Button to Show.', BUNCH_NAME ),
						),
					),
				);
//About Company
$bunch_sc['bunch_about_company']	=	array(
					"name" => __("About Company", BUNCH_NAME),
					"base" => "bunch_about_company",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Section About Company.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Bold Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Bold Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Normal Text', BUNCH_NAME ),
								   "param_name" => "text1",
								   "description" => __('Enter The Normal Text to Show.', BUNCH_NAME ),
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'services',
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Title', BUNCH_NAME ),
												   "param_name" => "ser_title",
												   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Text', BUNCH_NAME ),
												   "param_name" => "ser_text",
												   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
												),
											)
										),
										array(
										   "type" => "attach_image",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('About Image', BUNCH_NAME ),
										   "param_name" => "image",
										   "description" => __('Enter The About Image Url to Show.', BUNCH_NAME )
										),
									)
								);
//Our Projects
$bunch_sc['bunch_our_projects']	=	array(
					"name" => __("Our Projects", BUNCH_NAME),
					"base" => "bunch_our_projects",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Section Our Projects.', BUNCH_NAME),
					"params" => array(
					   			array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Excluded Categories ID', BUNCH_NAME ),
								   "param_name" => "exclude_cats",
								   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order By", BUNCH_NAME),
									"param_name" => "sort",
									'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order", BUNCH_NAME),
									"param_name" => "order",
									'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
//Architecture
$bunch_sc['bunch_architecture'] = array(
			"name" => __("Architecture", BUNCH_NAME),
			"base" => "bunch_architecture",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Architecture.', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', BUNCH_NAME ),
						   "param_name" => "bg_img",
						   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Architecture Style", BUNCH_NAME),
						   "param_name" => "testi_style",
						   'value' => array_flip(array('select'=>__('Select Architecture Styles', BUNCH_NAME),'style_one'=>__('Architecture Style One', BUNCH_NAME),'style_two'=>__('Architecture Style Two', BUNCH_NAME),'style_three'=>__('Architecture Style Three', BUNCH_NAME),'style_four'=>__('Architecture Style Four', BUNCH_NAME) ) ),			
						   "description" => __("Select the Testimonial Style.", BUNCH_NAME)
						),
					),
				);
//Our Funfacts
$bunch_sc['bunch_our_funfacts']	=	array(
					"name" => __("Our Funfacts", BUNCH_NAME),
					"base" => "bunch_our_funfacts",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show section Our Funfacts.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'funfact',
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Counter Value', BUNCH_NAME ),
													'param_name' => 'counter_value',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Title', BUNCH_NAME ),
													'param_name' => 'title',
												),
											)
										),
									)
								);
//Meet Our Team
$bunch_sc['bunch_meet_our_team'] = array(
			"name" => __("Meet Our Team", BUNCH_NAME),
			"base" => "bunch_meet_our_team",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Meet Our Team', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Latest Blog
$bunch_sc['bunch_latest_blog'] = array(
			"name" => __("Latest Blog", BUNCH_NAME),
			"base" => "bunch_latest_blog",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show section Latest Blog', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Our Testimonials
$bunch_sc['bunch_our_testimonials'] = array(
			"name" => __("Our Testimonials", BUNCH_NAME),
			"base" => "bunch_our_testimonials",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our Testimonials', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', BUNCH_NAME ),
						   "param_name" => "bg_img",
						   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Testimonial Style", BUNCH_NAME),
						   "param_name" => "testi_style",
						   'value' => array_flip(array('select'=>__('Select Testimonial Styles', BUNCH_NAME),'style_one'=>__('Testimonial Style One', BUNCH_NAME),'style_two'=>__('Testimonial Style Two', BUNCH_NAME),'style_three'=>__('Testimonial Style Three', BUNCH_NAME),'style_four'=>__('Testimonial Style Four', BUNCH_NAME),'style_five'=>__('Testimonial Style Five', BUNCH_NAME) ) ),			
						   "description" => __("Select the Testimonial Style.", BUNCH_NAME)
						),
					),
				);
//Our Partners
$bunch_sc['bunch_our_partners']	=	array(
					"name" => __("Our Partners", BUNCH_NAME),
					"base" => "bunch_our_partners",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Our Partners.', BUNCH_NAME),
					"params" => array(
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'partner',
									'params' => array(
												array(
												   "type" => "attach_image",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Partner Image', BUNCH_NAME ),
												   "param_name" => "image",
												   "description" => __('Enter The Partner Image Url to Show.', BUNCH_NAME )
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('External Link', BUNCH_NAME ),
												   "param_name" => "link",
												   "description" => __('Enter The External Link to Show.', BUNCH_NAME )
												),
											)
										),
									)
								);
//About Company 2
$bunch_sc['bunch_about_company_2']	=	array(
					"name" => __("About Company 2", BUNCH_NAME),
					"base" => "bunch_about_company_2",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Section About Company 2.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('About Image', BUNCH_NAME ),
								   "param_name" => "image",
								   "description" => __('Enter The About Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Bold Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Bold Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Normal Text', BUNCH_NAME ),
								   "param_name" => "text1",
								   "description" => __('Enter The Normal Text to Show.', BUNCH_NAME ),
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'services',
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Title', BUNCH_NAME ),
												   "param_name" => "ser_title",
												   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Text', BUNCH_NAME ),
												   "param_name" => "ser_text",
												   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
												),
											)
										),
									)
								);
//Our Projects 2
$bunch_sc['bunch_our_projects_2'] = array(
			"name" => __("Our Projects 2", BUNCH_NAME),
			"base" => "bunch_our_projects_2",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our Projects 2', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', BUNCH_NAME ),
						   "param_name" => "bg_img",
						   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Our Funfacts 2
$bunch_sc['bunch_our_funfacts_2']	=	array(
					"name" => __("Our Funfacts 2", BUNCH_NAME),
					"base" => "bunch_our_funfacts_2",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show section Our Funfacts 2.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'funfact',
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Counter Value', BUNCH_NAME ),
													'param_name' => 'counter_value',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Title', BUNCH_NAME ),
													'param_name' => 'title',
												),
											)
										),
									)
								);
//Our Faqs
$bunch_sc['bunch_our_faqs'] = array(
			"name" => __("Our Faqs", BUNCH_NAME),
			"base" => "bunch_our_faqs",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Our Faqs', BUNCH_NAME),
			"params" => array(
						array(
							'type' => 'param_group',
							'value' => '',
							'param_name' => 'faq_tab',
							"group" => esc_html__('Our Faqs', BUNCH_NAME ),
							'params' => array(
									array(
									   "type" => "textfield",
									   "holder" => "div",
									   "class" => "",
									   "heading" => __('Title', BUNCH_NAME ),
									   "param_name" => "title",
									   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
									),
									array(
									   "type" => "textarea",
									   "holder" => "div",
									   "class" => "",
									   "heading" => __('Text', BUNCH_NAME ),
									   "param_name" => "text",
									   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
									),
									array(
									   "type" => "textfield",
									   "holder" => "div",
									   "class" => "",
									   "heading" => __('Number', BUNCH_NAME ),
									   "param_name" => "num",
									   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME ),
									),
									array(
									   "type" => "textfield",
									   "holder" => "div",
									   "class" => "",
									   "heading" => __('Number Of Text', BUNCH_NAME ),
									   "param_name" => "text_limit",
									   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
									),
									array(
									   "type" => "dropdown",
									   "holder" => "div",
									   "class" => "",
									   "heading" => __( 'Category', BUNCH_NAME ),
									   "param_name" => "cat",
									   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'faqs_category', 'hide_empty' => FALSE ), true ) ),
									   "description" => __( 'Choose Category.', BUNCH_NAME ),
									),
									array(
									   "type" => "dropdown",
									   "holder" => "div",
									   "class" => "",
									   "heading" => __("Order By", BUNCH_NAME),
									   "param_name" => "sort",
									   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
									   "description" => __("Enter the sorting order.", BUNCH_NAME),
									),
									array(
									   "type" => "dropdown",
									   "holder" => "div",
									   "class" => "",
									   "heading" => __("Order", BUNCH_NAME),
									   "param_name" => "order",
									   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
									   "description" => __("Enter the sorting order.", BUNCH_NAME),
									),
								)
							),
							array(
							   "type" => "textfield",
							   "holder" => "div",
							   "class" => "",
							   "heading" => __('Form Title', BUNCH_NAME ),
							   "param_name" => "form_title",
							   "description" => __('Enter The Form Title to Show.', BUNCH_NAME ),
							   "group" => esc_html__('Ask Your Questions', BUNCH_NAME ),
							),
							array(
							   "type" => "textarea_raw_html",
							   "holder" => "div",
							   "class" => "",
							   "heading" => __('Contact Form 7', BUNCH_NAME ),
							   "param_name" => "contact_form",
							   "description" => __('Enter The Contact Form 7 Genrated Code to Show.', BUNCH_NAME ),
							   "group" => esc_html__('Ask Your Questions', BUNCH_NAME )
							),
						),
					);
//Meet Our Team 2
$bunch_sc['bunch_meet_our_team_2'] = array(
			"name" => __("Meet Our Team 2", BUNCH_NAME),
			"base" => "bunch_meet_our_team_2",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Meet Our Team 2', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Latest Blog 2
$bunch_sc['bunch_latest_blog_2'] = array(
			"name" => __("Latest Blog 2", BUNCH_NAME),
			"base" => "bunch_latest_blog_2",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show section Latest Blog 2', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//About Us
$bunch_sc['bunch_about_us']	=	array(
					"name" => __("About Us", BUNCH_NAME),
					"base" => "bunch_about_us",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Section About Us.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('About Image', BUNCH_NAME ),
								   "param_name" => "image",
								   "description" => __('Enter The About Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "content",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
									'type' => 'textfield',
									'value' => '',
									'heading' => esc_html__('Sub Title', BUNCH_NAME ),
									'param_name' => 'sub_title',
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'funfact',
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Counter Value', BUNCH_NAME ),
													'param_name' => 'counter_value',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Plus Sign', BUNCH_NAME ),
													'param_name' => 'plus_sign',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Title', BUNCH_NAME ),
													'param_name' => 'title1',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('External Link', BUNCH_NAME ),
													'param_name' => 'link',
												),
											)
										),
										array(
										   "type" => "textarea",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Text', BUNCH_NAME ),
										   "param_name" => "text1",
										   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Button Title', BUNCH_NAME ),
										   "param_name" => "btn_title",
										   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Button Link', BUNCH_NAME ),
										   "param_name" => "btn_link",
										   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
										),
									)
								);
//Call To Action
$bunch_sc['bunch_call_to_action']	=	array(
					"name" => __("Call To Action", BUNCH_NAME),
					"base" => "bunch_call_to_action",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Section Call To Action.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Title', BUNCH_NAME ),
								   "param_name" => "btn_title",
								   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
								),
							)
						);
//Our Best Services
$bunch_sc['bunch_our_best_services'] = array(
			"name" => __("Our Best Services", BUNCH_NAME),
			"base" => "bunch_our_best_services",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our Best Services', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Latest Projects
$bunch_sc['bunch_latest_projects'] = array(
			"name" => __("Latest Projects", BUNCH_NAME),
			"base" => "bunch_latest_projects",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Latest Projects', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', BUNCH_NAME ),
						   "param_name" => "bg_img",
						   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Latest Blog 3
$bunch_sc['bunch_latest_blog_3'] = array(
			"name" => __("Latest Blog 3", BUNCH_NAME),
			"base" => "bunch_latest_blog_3",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show section Latest Blog 3', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Our Funfacts 3
$bunch_sc['bunch_our_funfacts_3']	=	array(
					"name" => __("Our Funfacts 3", BUNCH_NAME),
					"base" => "bunch_our_funfacts_3",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show section Our Funfacts 3.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'funfact',
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Counter Value', BUNCH_NAME ),
													'param_name' => 'counter_value',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Title', BUNCH_NAME ),
													'param_name' => 'title',
												),
											)
										),
									)
								);
//About Company 3
$bunch_sc['bunch_about_company_3'] = array(
			"name" => __("About Company 3", BUNCH_NAME),
			"base" => "bunch_about_company_3",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section About Company 3.', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Bold Text', BUNCH_NAME ),
						   "param_name" => "bold_text",
						   "description" => __('Enter The Bold Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Normal Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Normal Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Meet Our Team 3
$bunch_sc['bunch_meet_our_team_3'] = array(
			"name" => __("Meet Our Team 3", BUNCH_NAME),
			"base" => "bunch_meet_our_team_3",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Meet Our Team 2', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
//Testimonials 3 Column
$bunch_sc['bunch_testimonials_3_col'] = array(
			"name" => __("Testimonials 3 Column", BUNCH_NAME),
			"base" => "bunch_testimonials_3_col",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our Testimonials', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
				
//Our Services Style 4
$bunch_sc['bunch_services_v4']	=	array(
					"name" => __("Services v1", BUNCH_NAME),
					"base" => "bunch_services_v4",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Services v1.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
							)
						);	
						
//Our Services Style 4
$bunch_sc['bunch_we_are_construction']	=	array(
					"name" => __("We are construction", BUNCH_NAME),
					"base" => "bunch_we_are_construction",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show We are construction.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter Title of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter Text of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Feature Image', BUNCH_NAME ),
								   "param_name" => "image",
								   "description" => __('Enter Image of Items to Show.', BUNCH_NAME ),
								),
							)
						);							
						
//Our Services Style 4
$bunch_sc['bunch_slider_services']	=	array(
					"name" => __("Slider Services", BUNCH_NAME),
					"base" => "bunch_slider_services",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Slider Services.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('BG image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background image.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "content",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter Text of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
							)
						);	
						
//Meet Our Team
$bunch_sc['bunch_meet_our_team_4'] = array(
			"name" => __("Meet Our Team 4", BUNCH_NAME),
			"base" => "bunch_meet_our_team_4",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Meet Our Team 4', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);
				
//Meet Our Team
$bunch_sc['bunch_call_out'] = array(
			"name" => __("Call Out", BUNCH_NAME),
			"base" => "bunch_call_out",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Section Call Out', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('BG Image', BUNCH_NAME ),
						   "param_name" => "bg_img",
						   "description" => __('Enter The Background Image to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Btn Link', BUNCH_NAME ),
						   "param_name" => "btn_link",
						   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Btn Title', BUNCH_NAME ),
						   "param_name" => "btn_title",
						   "description" => __('Enter The Button Title to Show.', BUNCH_NAME )
						),
					),
				);																

//Our Testimonials
$bunch_sc['bunch_our_testimonial_v2'] = array(
			"name" => __("Our Testimonials V2", BUNCH_NAME),
			"base" => "bunch_our_testimonial_v2",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our Testimonials V2', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', BUNCH_NAME ),
						   "param_name" => "bg_img",
						   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Testimonial Style", BUNCH_NAME),
						   "param_name" => "testi_style",
						   'value' => array_flip(array('select'=>__('Select Testimonial Styles', BUNCH_NAME),'style_one'=>__('Testimonial Style One', BUNCH_NAME),'style_two'=>__('Testimonial Style Two', BUNCH_NAME),'style_three'=>__('Testimonial Style Three', BUNCH_NAME),'style_four'=>__('Testimonial Style Four', BUNCH_NAME),'style_five'=>__('Testimonial Style Five', BUNCH_NAME) ) ),			
						   "description" => __("Select the Testimonial Style.", BUNCH_NAME)
						),
					),
				);	
				
//Our Services Style 5
$bunch_sc['bunch_services_v5']	=	array(
					"name" => __("Services V5", BUNCH_NAME),
					"base" => "bunch_services_v5",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Services v5.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Bg Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter Back Ground of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
							)
						);	
						
//Our Funfacts 4
$bunch_sc['bunch_our_funfacts_4']	=	array(
					"name" => __("Fun Facts 4", BUNCH_NAME),
					"base" => "bunch_our_funfacts_4",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Fun Facts 4.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Bg Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter Back Ground of Items to Show.', BUNCH_NAME ),
								),
								
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'funfact_four',
									'params' => array(
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Counter Value', BUNCH_NAME ),
													'param_name' => 'counter_value',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Title', BUNCH_NAME ),
													'param_name' => 'title',
												),
												)
								),
								
							)
						);	
						
//Our Services Style 6
$bunch_sc['bunch_services_v6']	=	array(
					"name" => __("Services V6", BUNCH_NAME),
					"base" => "bunch_services_v6",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Services v6.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Bg Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter Back Ground of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter Title of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter Sub Title of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter Text of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter Button Link of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text', BUNCH_NAME ),
								   "param_name" => "btn_text",
								   "description" => __('Enter Button Text of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link1",
								   "description" => __('Enter Button Link of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text', BUNCH_NAME ),
								   "param_name" => "btn_text1",
								   "description" => __('Enter Button Text of Items to Show.', BUNCH_NAME ),
								),
							)
						);	

//************Home 6 Shortcodes**********
//best services
$bunch_sc['bunch_our_best_services_v2']	=	array(
					"name" => __("Best Services V2", BUNCH_NAME),
					"base" => "bunch_our_best_services_v2",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Best Services V2.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Content', BUNCH_NAME ),
								   "param_name" => "content",
								   "description" => __('Enter The Content to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Title', BUNCH_NAME ),
								   "param_name" => "btn_title",
								   "description" => __('Enter The Button Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link1",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Title', BUNCH_NAME ),
								   "param_name" => "btn_title1",
								   "description" => __('Enter The Button Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Feature Image', BUNCH_NAME ),
								   "param_name" => "img",
								   "description" => __('Enter The Feature Image Url to Show.', BUNCH_NAME )
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'best_services',
									'params' => array(
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Title', BUNCH_NAME ),
												   "param_name" => "b_title",
												   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Text', BUNCH_NAME ),
												   "param_name" => "b_text",
												   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
												),
											)
										),
									)
								);

//Our Project
$bunch_sc['bunch_our_projects_5']	=	array(
					"name" => __("Featured Projects _v2", BUNCH_NAME),
					"base" => "bunch_our_projects_5",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Featured Projects V2.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter Title of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter Text of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
							)
						);	

//best services
$bunch_sc['bunch_services_v7']	=	array(
					"name" => __("Services V7", BUNCH_NAME),
					"base" => "bunch_services_v7",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Services V7.', BUNCH_NAME),
					"params" => array(
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'services_v7',
									'params' => array(
												array(
												   "type" => "attach_image",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Feature Image', BUNCH_NAME ),
												   "param_name" => "img",
												   "description" => __('Enter The Feature Image to Show.', BUNCH_NAME )
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Title', BUNCH_NAME ),
												   "param_name" => "title",
												   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Text', BUNCH_NAME ),
												   "param_name" => "text",
												   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Feature List', BUNCH_NAME ),
												   "param_name" => "content",
												   "description" => __('Enter The Feature List to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Button Link', BUNCH_NAME ),
												   "param_name" => "btn_link",
												   "description" => __('Enter The Button Link to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Button Title', BUNCH_NAME ),
												   "param_name" => "btn_title",
												   "description" => __('Enter The Button Title to Show.', BUNCH_NAME ),
												),
											)
										),
									)
								);	

//best services
$bunch_sc['bunch_meet_our_team_5']	=	array(
					"name" => __("Our Team v5", BUNCH_NAME),
					"base" => "bunch_meet_our_team_5",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Our Team v5.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
						
//Call To Action
$bunch_sc['bunch_company_network']	=	array(
					"name" => __("Company Network", BUNCH_NAME),
					"base" => "bunch_company_network",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Section Company Network.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Title', BUNCH_NAME ),
								   "param_name" => "btn_title",
								   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter The Contact Us Button to Show.', BUNCH_NAME ),
								),
							)
						);	
						
//Our Services Style 6
$bunch_sc['bunch_our_projects_4']	=	array(
					"name" => __("Featured Projects", BUNCH_NAME),
					"base" => "bunch_our_projects_4",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Featured Projects.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter Title of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter Text of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								),
							)
						);
						
//Our Testimonials 5
$bunch_sc['bunch_our_testimonials_5'] = array(
			"name" => __("Testimonials v5", BUNCH_NAME),
			"base" => "bunch_our_testimonials_5",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our Testimonials v5', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', BUNCH_NAME ),
						   "param_name" => "bg_img",
						   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Sub Title', BUNCH_NAME ),
						   "param_name" => "sub_title",
						   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number Of Text', BUNCH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);	
				
//Our Testimonials 5
$bunch_sc['bunch_call_to_action_v5'] = array(
			"name" => __("Call To Action v5", BUNCH_NAME),
			"base" => "bunch_call_to_action_v5",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our Call To Action v5', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Link', BUNCH_NAME ),
						   "param_name" => "btn_link",
						   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Title', BUNCH_NAME ),
						   "param_name" => "btn_title",
						   "description" => __('Enter Button Title to Show.', BUNCH_NAME )
						),
					),
				);	
				
//Latest Blog
$bunch_sc['bunch_blog_v5'] = array(
			"name" => __("Blog v5", BUNCH_NAME),
			"base" => "bunch_blog_v5",
			"class" => "",
			"category" => __('Construct Zilla', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show section Blog v5', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Sub Title', BUNCH_NAME ),
						   "param_name" => "sub_title",
						   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', BUNCH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', BUNCH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', BUNCH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", BUNCH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", BUNCH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", BUNCH_NAME)
						),
					),
				);	
				
//Perallex information
$bunch_sc['bunch_perallex_info']	=	array(
					"name" => __("Perallex Information", BUNCH_NAME),
					"base" => "bunch_perallex_info",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show section Perallex Information.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter The Background Image Url to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'funfact',
									'params' => array(
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Title', BUNCH_NAME ),
													'param_name' => 'title',
												),
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Info', BUNCH_NAME ),
													'param_name' => 'info',
												),
											)
										),
									)
								);																																																		
				
//Our Services
$bunch_sc['bunch_our_services']	=	array(
					"name" => __("Our Services", BUNCH_NAME),
					"base" => "bunch_our_services",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Our Services.', BUNCH_NAME),
					"params" => array(
								array(
								   'type' => 'dropdown',
								   'heading' => esc_html__( 'Choose Sidebar', BUNCH_NAME ),
								   "param_name" => 'sidebar_slug',
								   "value" => bheem_bunch_get_sidebars( true ),
								   'description' => esc_html__( 'Choose Sidebar.', BUNCH_NAME ),
							   	),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
							)
						);
//Service Single One
$bunch_sc['bunch_service_single_1']	=	array(
					"name" => __("Service Single One", BUNCH_NAME),
					"base" => "bunch_service_single_1",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Service Single One.', BUNCH_NAME),
					"params" => array(
								array(
								   'type' => 'dropdown',
								   'heading' => esc_html__( 'Choose Sidebar', BUNCH_NAME ),
								   "param_name" => 'sidebar_slug',
								   "value" => bheem_bunch_get_sidebars( true ),
								   'description' => esc_html__( 'Choose Sidebar.', BUNCH_NAME ),
							   	),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image One', BUNCH_NAME ),
								   "param_name" => "ser_img1",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image Two', BUNCH_NAME ),
								   "param_name" => "ser_img2",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image Three', BUNCH_NAME ),
								   "param_name" => "ser_img3",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Description', BUNCH_NAME ),
								   "param_name" => "ser_des",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Next Description', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('External Link', BUNCH_NAME ),
								   "param_name" => "link",
								   "description" => __('Enter The External Link to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
							)
						);
//Service Single Two
$bunch_sc['bunch_service_single_2']	=	array(
					"name" => __("Service Single Two", BUNCH_NAME),
					"base" => "bunch_service_single_2",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Service Single Two.', BUNCH_NAME),
					"params" => array(
								array(
								   'type' => 'dropdown',
								   'heading' => esc_html__( 'Choose Sidebar', BUNCH_NAME ),
								   "param_name" => 'sidebar_slug',
								   "value" => bheem_bunch_get_sidebars( true ),
								   'description' => esc_html__( 'Choose Sidebar.', BUNCH_NAME ),
							   	),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image One', BUNCH_NAME ),
								   "param_name" => "ser_img1",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Description', BUNCH_NAME ),
								   "param_name" => "ser_des",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image Two', BUNCH_NAME ),
								   "param_name" => "ser_img2",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image Three', BUNCH_NAME ),
								   "param_name" => "ser_img3",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Next Description', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('External Link', BUNCH_NAME ),
								   "param_name" => "link",
								   "description" => __('Enter The External Link to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
							)
						);
//Service Single Three
$bunch_sc['bunch_service_single_3']	=	array(
					"name" => __("Service Single Three", BUNCH_NAME),
					"base" => "bunch_service_single_3",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Service Single Three.', BUNCH_NAME),
					"params" => array(
								array(
								   'type' => 'dropdown',
								   'heading' => esc_html__( 'Choose Sidebar', BUNCH_NAME ),
								   "param_name" => 'sidebar_slug',
								   "value" => bheem_bunch_get_sidebars( true ),
								   'description' => esc_html__( 'Choose Sidebar.', BUNCH_NAME ),
							   	),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image One', BUNCH_NAME ),
								   "param_name" => "ser_img1",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image Two', BUNCH_NAME ),
								   "param_name" => "ser_img2",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Description', BUNCH_NAME ),
								   "param_name" => "ser_des",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sevrice Image Three', BUNCH_NAME ),
								   "param_name" => "ser_img3",
								   "description" => __('Enter The Sevrice Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Next Description', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('External Link', BUNCH_NAME ),
								   "param_name" => "link",
								   "description" => __('Enter The External Link to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Services', BUNCH_NAME),
								),
							)
						);
//Our Projects 3
$bunch_sc['bunch_our_projects_3']	=	array(
					"name" => __("Our Projects 3", BUNCH_NAME),
					"base" => "bunch_our_projects_3",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Section Our Projects 3.', BUNCH_NAME),
					"params" => array(
					   			array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Excluded Categories ID', BUNCH_NAME ),
								   "param_name" => "exclude_cats",
								   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order By", BUNCH_NAME),
									"param_name" => "sort",
									'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order", BUNCH_NAME),
									"param_name" => "order",
									'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
//Project Details
$bunch_sc['bunch_project_detail']	=	array(
					"name" => __("Project Details", BUNCH_NAME),
					"base" => "bunch_project_detail",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Project Details.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Project Image One', BUNCH_NAME ),
								   "param_name" => "proj_img1",
								   "description" => __('Enter The Project Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Description', BUNCH_NAME ),
								   "param_name" => "proj_des",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Project Image Two', BUNCH_NAME ),
								   "param_name" => "proj_img2",
								   "description" => __('Enter The Project Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Project Image Three', BUNCH_NAME ),
								   "param_name" => "proj_img3",
								   "description" => __('Enter The Project Image Url to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Next Description', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('External Link', BUNCH_NAME ),
								   "param_name" => "link",
								   "description" => __('Enter The External Link to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME),
								),
								array(
								   'type' => 'dropdown',
								   'heading' => esc_html__( 'Choose Sidebar', BUNCH_NAME ),
								   "param_name" => 'sidebar_slug',
								   "value" => bheem_bunch_get_sidebars( true ),
								   'description' => esc_html__( 'Choose Sidebar.', BUNCH_NAME ),
								   "group" => esc_html__('Widget Sidebar', BUNCH_NAME),
							   	),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Our Projects', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								   "group" => esc_html__('Our Projects', BUNCH_NAME),
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order By", BUNCH_NAME),
									"param_name" => "sort",
									'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME),
									"group" => esc_html__('Our Projects', BUNCH_NAME),
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order", BUNCH_NAME),
									"param_name" => "order",
									'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME),
									"group" => esc_html__('Our Projects', BUNCH_NAME),
								),
							)
						);
//Blog List View
$bunch_sc['bunch_blog_list_view']	=	array(
					"name" => __("Blog List View", BUNCH_NAME),
					"base" => "bunch_blog_list_view",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Blog List View.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order By", BUNCH_NAME),
									"param_name" => "sort",
									'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order", BUNCH_NAME),
									"param_name" => "order",
									'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   'type' => 'dropdown',
								   'heading' => esc_html__( 'Choose Sidebar', BUNCH_NAME ),
								   "param_name" => 'sidebar_slug',
								   "value" => bheem_bunch_get_sidebars( true ),
								   'description' => esc_html__( 'Choose Sidebar.', BUNCH_NAME ),
								   "group" => esc_html__('Select Blog Widgets', BUNCH_NAME),
							   	),
							)
						);
//Blog Grid View
$bunch_sc['bunch_blog_grid_view']	=	array(
					"name" => __("Blog Grid View", BUNCH_NAME),
					"base" => "bunch_blog_grid_view",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Blog Grid View.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number Of Text', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Number Of Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order By", BUNCH_NAME),
									"param_name" => "sort",
									'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
									"type" => "dropdown",
									"holder" => "div",
									"class" => "",
									"heading" => __("Order", BUNCH_NAME),
									"param_name" => "order",
									'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),   
									"description" => __("Enter the sorting order.", BUNCH_NAME),
								),
								array(
								   'type' => 'dropdown',
								   'heading' => esc_html__( 'Choose Sidebar', BUNCH_NAME ),
								   "param_name" => 'sidebar_slug',
								   "value" => bheem_bunch_get_sidebars( true ),
								   'description' => esc_html__( 'Choose Sidebar.', BUNCH_NAME ),
								   "group" => esc_html__('Select Blog Widgets', BUNCH_NAME),
							   	),
							)
						);
//Contact Us
$bunch_sc['bunch_contact_us']	=	array(
					"name" => __("Contact Us", BUNCH_NAME),
					"base" => "bunch_contact_us",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Contact Us.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Form Title', BUNCH_NAME ),
								   "param_name" => "form_title",
								   "description" => __('Enter The Form Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME )
								),
								array(
								   "type" => "textarea_raw_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Contact Form 7', BUNCH_NAME ),
								   "param_name" => "contact_form",
								   "description" => __('Enter The Contact Form 7 Genrated Code to Show.', BUNCH_NAME ),
								   "group" => esc_html__('General', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Contact Info', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   "group" => esc_html__('Contact Info', BUNCH_NAME )
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'service',
									"group" => esc_html__('Contact Info', BUNCH_NAME ),
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Title', BUNCH_NAME ),
												   "param_name" => "title1",
												   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Text', BUNCH_NAME ),
												   "param_name" => "text1",
												   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('External Link', BUNCH_NAME ),
												   "param_name" => "link",
												   "description" => __('Enter The External Link to Show.', BUNCH_NAME ),
												),
											)
										),
										array(
										   "type" => "checkbox",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Show Or Hide Social Icons', BUNCH_NAME ),
										   "param_name" => "show_social",
										   "description" => __('Enter The Show Or Hide Social Icons.', BUNCH_NAME ),
										   "group" => esc_html__('Contact Info', BUNCH_NAME ),
										),
									)
								);
//Google Map
$bunch_sc['bunch_google_map']	=	array(
					"name" => __("Google Map", BUNCH_NAME),
					"base" => "bunch_google_map",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Google Map.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Latitude', BUNCH_NAME ),
								   "param_name" => "lat",
								   "description" => __('Enter The Latitude to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Longitude', BUNCH_NAME ),
								   "param_name" => "long",
								   "description" => __('Enter The Longitude to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Map Zoom', BUNCH_NAME ),
								   "param_name" => "map_zoom",
								   "description" => __('Enter The Map Zoom to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Mark Title', BUNCH_NAME ),
								   "param_name" => "mark_title",
								   "description" => __('Enter The Mark Title to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Mark Address', BUNCH_NAME ),
								   "param_name" => "address",
								   "description" => __('Enter The Mark Address to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Mark Email', BUNCH_NAME ),
								   "param_name" => "mark_email",
								   "description" => __('Enter The Mark Email to Show.', BUNCH_NAME ),
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Marker Image', BUNCH_NAME ),
								   "param_name" => "img",
								   "description" => __('Enter The Marker Image Url to Show.', BUNCH_NAME )
								),
							)
						);
//Contact Us 2
$bunch_sc['bunch_contact_us_2']	=	array(
					"name" => __("Contact Us 2", BUNCH_NAME),
					"base" => "bunch_contact_us_2",
					"class" => "",
					"category" => __('Construct Zilla', BUNCH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Contact Us 2.', BUNCH_NAME),
					"params" => array(
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'service',
									"group" => esc_html__('Contact Info', BUNCH_NAME ),
									'params' => array(
												array(
													"type" => "dropdown",
													'value' => '',
													"heading" => __("Icon", BUNCH_NAME),
													"param_name" => "icons",
													"value" => (array)vp_get_fontawesome_icons(),
													"description" => __("Choose Icon for Process", BUNCH_NAME)
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Title', BUNCH_NAME ),
												   "param_name" => "title1",
												   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textarea",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('Text', BUNCH_NAME ),
												   "param_name" => "text1",
												   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
												),
												array(
												   "type" => "textfield",
												   "holder" => "div",
												   "class" => "",
												   "heading" => __('External Link', BUNCH_NAME ),
												   "param_name" => "link",
												   "description" => __('Enter The External Link to Show.', BUNCH_NAME ),
												),
											)
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Form Title', BUNCH_NAME ),
										   "param_name" => "form_title",
										   "description" => __('Enter The Form Title to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Contact Form', BUNCH_NAME )
										),
										array(
										   "type" => "textarea_raw_html",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Contact Form 7', BUNCH_NAME ),
										   "param_name" => "contact_form",
										   "description" => __('Enter The Contact Form 7 Genrated Code to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Contact Form', BUNCH_NAME )
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Latitude', BUNCH_NAME ),
										   "param_name" => "lat",
										   "description" => __('Enter The Latitude to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Google Map', BUNCH_NAME )
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Longitude', BUNCH_NAME ),
										   "param_name" => "long",
										   "description" => __('Enter The Longitude to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Google Map', BUNCH_NAME )
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Map Zoom', BUNCH_NAME ),
										   "param_name" => "map_zoom",
										   "description" => __('Enter The Map Zoom to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Google Map', BUNCH_NAME )
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Mark Title', BUNCH_NAME ),
										   "param_name" => "mark_title",
										   "description" => __('Enter The Mark Title to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Google Map', BUNCH_NAME )
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Mark Address', BUNCH_NAME ),
										   "param_name" => "address",
										   "description" => __('Enter The Mark Address to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Google Map', BUNCH_NAME )
										),
										array(
										   "type" => "textfield",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Mark Email', BUNCH_NAME ),
										   "param_name" => "mark_email",
										   "description" => __('Enter The Mark Email to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Google Map', BUNCH_NAME )
										),
										array(
										   "type" => "attach_image",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __('Marker Image', BUNCH_NAME ),
										   "param_name" => "img",
										   "description" => __('Enter The Marker Image Url to Show.', BUNCH_NAME ),
										   "group" => esc_html__('Google Map', BUNCH_NAME )
										),
									)
								);

																																																								
/*----------------------------------------------------------------------------*/
$bunch_sc = apply_filters( '_bunch_shortcodes_array', $bunch_sc );
	
return $bunch_sc;