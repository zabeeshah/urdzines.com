<div class="search-bx">
    <form role="search" action="<?php echo esc_url(home_url('/')); ?>" method="get">
        <div class="input-group">
            <input name="s" value="" type="text" class="form-control" placeholder="<?php esc_attr_e('Search here ...', 'bheem');?>">
            <span class="input-group-btn">
            <button type="submit" class="site-button"><i class="fa fa-search"></i></button>
            </span> 
        </div>
    </form>
</div>