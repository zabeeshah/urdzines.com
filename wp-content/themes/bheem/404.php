<?php
	get_header();
	$options = _WSH()->option(); 
	$bg = bheem_set($options, 'page_bg_img')? bheem_set($options, 'page_bg_img') : get_template_directory_uri().'/images/banner/bnr1.jpg';
	$title = bheem_set($options, '404_page_title');
	$page_heading = bheem_set($options, '404_page_heading');
	$page_text = bheem_set($options, '404_page_text');
     
?>

<!-- inner page banner -->
<div class="dez-bnr-inr overlay-black-middle" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="dez-bnr-inr-entry">
            <h1 class="text-white"><?php if($title) echo wp_kses_post($title); else wp_title('');?></h1>
        </div>
    </div>
</div>
<!-- inner page banner END -->
<!-- Breadcrumb row -->
<div class="breadcrumb-row">
    <div class="container">
        <?php echo wp_kses_post(bheem_get_the_breadcrumb()); ?>
    </div>
</div>
<!-- Breadcrumb row END -->

<!-- 404 Page -->
<div class="section-content">
    <div class="row">
        <div class="col-md-12">
            <div class="page-notfound text-center">
                <form method="post">
                    <strong><?php if($page_heading) echo wp_kses_post($page_heading); else esc_html_e('404', 'bheem'); ?></strong>
                    <h5 class="m-b20 text-uppercase">
                    	<?php if($page_text) echo wp_kses_post($page_text); else esc_html_e('Page not found', 'bheem'); ?>
                    </h5>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-button-secondry"><span><?php esc_html_e('Go To Home', 'bheem'); ?></span><i class="fa fa-angle-right m-l15"></i></a>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 404 Page END -->
 		
<?php get_footer(); ?>