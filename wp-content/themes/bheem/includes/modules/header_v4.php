<?php $options = _WSH()->option();
	bheem_bunch_global_variable();
?>

<!-- header -->
<header class="site-header header header-style-1">
    
    <!-- main header -->
    <div class="sticky-header main-bar-wraper">
        <div class="main-bar clearfix ">
            <div class="container clearfix">
                <!-- website logo -->
                <div class="logo-header mostion dark">
                	<?php if(bheem_set($options, 'logo_image_2')):?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(bheem_set($options, 'logo_image_2'));?>" alt="" title="<?php esc_html_e('Bheem', 'bheem');?>"></a>
                    <?php else:?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo-black.png');?>" alt="<?php esc_html_e('Bheem', 'bheem');?>"></a>
                    <?php endif;?>
                </div>
                <!-- nav toggle button -->
                <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed"> <span class="sr-only"><?php esc_html_e('Toggle navigation', 'bheem'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <!-- extra nav -->
                <div class="extra-nav">
                    <div class="extra-cell">
                        <button id="quik-search-btn" type="button" class="site-button"><i class="fa fa-search"></i></button>
						
						<?php if(bheem_set($options, 'cart_button_4')): ?>
						<div class="shop-cart navbar-right">
							<?php if(bheem_is_woocommerce_active() && is_active_sidebar('header-cart')) : 
									global $woocommerce;
									ob_start();
							?>
								<a class="site-button-link cart-btn" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php esc_attr_e('View your shopping cart', 'bheem'); ?>"><i class="fa fa-shopping-bag"></i><span class="badge bg-black cart-count"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'bheem'), $woocommerce->cart->cart_contents_count);?></span></a>
								<ul class="minicart">
								   <?php  dynamic_sidebar('header-cart'); ?>
								</ul>
                            <?php endif; ?>
						</div>
						<?php endif; ?>
                    </div>
                </div>
                <!-- Quik search -->
                <div class="dez-quik-search bg-primary">
                    <?php get_template_part('searchform2')?>
                </div>
                <!-- main nav -->
                <div class="header-nav navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
							'container_class'=>'navbar-collapse collapse navbar-right',
							'menu_class'=>'nav navbar-nav',
							'fallback_cb'=>false, 
							'items_wrap' => '%3$s', 
							'container'=>false,
							'walker'=> new Bunch_Bootstrap_walker()  
						) ); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- main header END -->
</header>
<!-- header END -->