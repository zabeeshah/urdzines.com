<?php $options = _WSH()->option();
	bheem_bunch_global_variable();
?>

<!-- header -->
<header class="site-header header header-style-5">
    
    <?php if(bheem_set($options, 'head_social_2')): ?>
    	<?php if(bheem_set( $options, 'social_media' ) && is_array( bheem_set( $options, 'social_media' ) )): ?>
            <!-- top bar -->
            <div class="top-bar ">
                <div class="container">
                    <div class="row">
                        <div class="dez-topbar-left"> </div>
                        <div class="dez-topbar-right">
                            <ul class="social-bx list-inline pull-right">
                                <?php $social_icons = bheem_set( $options, 'social_media' );
									foreach( bheem_set( $social_icons, 'social_media' ) as $social_icon ):
									if( isset( $social_icon['tocopy' ] ) ) continue; ?>
									<li><a href="<?php echo esc_url(bheem_set( $social_icon, 'social_link')); ?>" class="fa <?php echo esc_attr(bheem_set( $social_icon, 'social_icon')); ?>"></a></li>
								<?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- top bar END-->
    	<?php endif;?>
    <?php endif;?>
    
    <!-- main header -->
    <div class="sticky-header header-curve main-bar-wraper">
        <div class="main-bar clearfix ">
            <div class="container clearfix">
                
                <!-- website logo -->
                <div class="logo-header mostion">
                	<?php if(bheem_set($options, 'logo_image')):?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(bheem_set($options, 'logo_image'));?>" alt="<?php esc_attr_e('image', 'bheem');?>" title="<?php esc_attr_e('Bheem', 'bheem');?>"></a>
                    <?php else:?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo.png');?>" alt="<?php esc_attr_e('Bheem', 'bheem');?>"></a>
                    <?php endif;?>
                </div>
                <!-- nav toggle button -->
                <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed"> <span class="sr-only"><?php esc_html_e('Toggle navigation', 'bheem'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <!-- extra nav -->
                <div class="extra-nav">
                    <div class="extra-cell">
                        <button id="quik-search-btn" type="button" class="site-button"><i class="fa fa-search"></i></button>
						
						<?php if(bheem_set($options, 'cart_button_2')): ?>
						<div class="shop-cart navbar-right">
							<?php if(bheem_is_woocommerce_active() && is_active_sidebar('header-cart')) : 
									global $woocommerce;
									ob_start();
							?>
								<a class="site-button cart-btn btn-sm" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php esc_attr_e('View your shopping cart', 'bheem'); ?>"><i class="fa fa-shopping-bag"></i><span class="badge bg-black cart-count"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'bheem'), $woocommerce->cart->cart_contents_count);?></span></a>
								<ul class="minicart">
								   <?php  dynamic_sidebar('header-cart'); ?>
								</ul>
                            <?php endif; ?>
						</div>
						<?php endif; ?>
                    </div>
                </div>
                <!-- Quik search -->
                <div class="dez-quik-search bg-primary">
                    <?php get_template_part('searchform2')?>
                </div>
                <!-- main nav -->
                <div class="header-nav navbar-collapse collapse">
                    <ul class=" nav navbar-nav">
                        <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
							'container_class'=>'navbar-collapse collapse navbar-right',
							'menu_class'=>'nav navbar-nav',
							'fallback_cb'=>false, 
							'items_wrap' => '%3$s', 
							'depth' => 3,
							'container'=>false,
							'walker'=> new Bunch_Bootstrap_walker()  
						) ); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- main header END -->
</header>
<!-- header END -->