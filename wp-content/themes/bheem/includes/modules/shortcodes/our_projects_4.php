<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['projects_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- Featured projects -->
<div class="section-full content-inner bg-white">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="title-top"><?php echo wp_kses_post($title);?></h2>
            <h2 class="h1 text-secondry"><?php echo wp_kses_post($text);?></h2>
        </div>
        <div class="row">
            
			<?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$projects_meta = _WSH()->get_meta();
			?>
            
            <div class="col-md-4 m-b30">
                <div class="fea-project">	
                    <div class="project-img">
                        <?php the_post_thumbnail('bheem_700x900'); ?>
                        <div class="project-content text-white">
                        	<?php $term_list = wp_get_post_terms(get_the_id(), 'projects_category', array("fields" => "names")); ?>
                            <a href="#" class="site-button radius-no text-secondry m-b30 button-sm"><?php echo implode( ', ', (array)$term_list );?></a>
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                            <div class="project-link">
                                <span class="pull-left"><a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>" class="site-button-link"><?php esc_html_e('Read More', 'bheem')?></a></span>
                                <ul class="list-inline m-a0 pull-right">
                                    <li><a><i class="ti-world"></i></a></li>
                                    <li><a><i class="ti-link"></i></a></li>
                                    <li><a><i class="ti-target"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <h2 class="m-b10"><a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_title();?></a></h2>
                        <p class="m-a0"><?php echo wp_kses_post(bheem_set($projects_meta, 'designation'));?></p>
                    </div>
                </div>
            </div>
            
            <?php endwhile; ?>
            
        </div>
    </div>
</div>

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>