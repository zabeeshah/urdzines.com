<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- About Company -->
<div class="section-full content-inner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-head text-left">
                    <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
                    <div class="aon-separator bg-primary"></div>
                    <p><strong><?php echo wp_kses_post($bold_text); ?></strong></p>
                    <p class="m-b50"><?php echo wp_kses_post($text); ?></p>
                </div>
                <div class="row">
                    <?php while($query->have_posts()): $query->the_post();
						global $post ; 
						$services_meta = _WSH()->get_meta();
					?>
                    <div class="col-md-4 col-sm-6">
                        <div class="icon-bx-wraper bx-style-2 m-l40 m-b30 p-a30 left">
                            <div class="icon-bx-sm bg-primary radius"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>" class="icon-cell"><i class="fa <?php echo str_replace("icon ", "",bheem_set($services_meta, 'services_icon'));?>"></i></a> </div>
                            <div class="icon-content p-l40">
                                <h5 class="aon-tilte text-uppercase"><?php the_title(); ?></h5>
                                <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Company END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>