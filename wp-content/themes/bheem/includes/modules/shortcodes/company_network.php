<?php
ob_start() ;?>

<div class="section-full content-inner-1" style="background-image: url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>'); background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 text-secondry">
                <h2 class="title-top"><?php echo wp_kses_post($title); ?></h2>
                <h2 class="font-50"><?php echo wp_kses_post($sub_title); ?></h2>
                <p class="m-t20 max-w500"><?php echo wp_kses_post($text); ?></p>
                <a href="<?php echo esc_url($btn_link); ?>" class="site-button text-secondry"><?php echo wp_kses_post($btn_title); ?></a>
            </div>
        </div>
    </div>
</div>

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>