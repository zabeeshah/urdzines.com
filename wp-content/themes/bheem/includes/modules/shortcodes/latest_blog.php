<?php  
   global $post ;
   $count = 0;
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>  
<?php if($query->have_posts()):  ?>   

<!-- Latest blog -->
<div class="section-full content-inner ">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
        </div>
        <div class="section-content ">
            <div class="equal-wraper">
                <div class="blog-carousel">
					<?php while($query->have_posts()): $query->the_post();
                        global $post ; 
                        $post_meta = _WSH()->get_meta();
                    ?>
                    <div class="m-b10">
                        <div class="blog-post latest-blog-1 date-style-3 skew-date">
                            <div class="dez-post-media dez-img-effect zoom-slow"> <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_post_thumbnail('bheem_700x438'); ?></a> </div>
                            <div class="dez-post-info p-t20">
                                <div class="dez-post-title ">
                                    <h3 class="post-title"><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
                                </div>
                                <div class="dez-post-meta ">
                                    <ul>
                                        <li class="post-date"> <i class="fa fa-calendar"></i><strong><?php echo get_the_date(); ?></strong> </li>
                                        <li class="post-author"><i class="fa fa-user"></i><?php esc_html_e('By', 'bheem'); ?> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a> </li>
                                        <li class="post-comment"><i class="fa fa-comments"></i> <a href="<?php echo esc_url(get_the_permalink(get_the_id()).'#comment');?>"><?php comments_number( wp_kses_post(__('0 Comments' , 'bheem')), wp_kses_post(__('1 Comment' , 'bheem')), wp_kses_post(__('% Comments' , 'bheem'))); ?></a> </li>
                                    </ul>
                                </div>
                                <div class="dez-post-text">
                                    <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Latest blog END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>