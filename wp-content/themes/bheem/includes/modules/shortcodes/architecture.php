<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<?php if ( $testi_style == 'style_two' ) : ?>

<!-- Architecture -->
<div class="section-full bg-white content-inner" >
    <div class="container">
        <div class="section-head text-center">
            <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <div class="col-md-4 col-sm-6">
                <div class="icon-bx-wraper bx-style-1 p-a30 center m-b30">
                    <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i class="fa <?php echo str_replace("icon ", "",bheem_set($services_meta, 'services_icon'));?> text-primary"></i></span> </div>
                    <div class="icon-content">
                        <h5 class="dez-tilte text-uppercase"><?php the_title(); ?></h5>
                        <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</div>
<!-- Architecture END-->

<?php elseif ( $testi_style == 'style_three' ) : ?>

<!-- Architecture -->
<div class="section-full text-center aon-our-team bg-white content-inner-1">
    <div class="container">
        <div class="section-head">
            <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
            <span class="title-small"><?php echo wp_kses_post($text); ?></span>
            <div class="after-titile-line"></div>
        </div>
        <div class="section-content">
            <div class="row">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$services_meta = _WSH()->get_meta();
				?>
                <?php if(($count%3) == 0 && $count != 0):?>
            </div>
            <div class="row p-t50">
                <?php endif; ?>
                <div class="col-md-4 col-sm-4">
                    <div class="icon-bx-wraper center">
                        <div class="icon-bx-sm radius bg-primary m-b20"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>" class="icon-cell"><i class="fa <?php echo str_replace("icon ", "",bheem_set($services_meta, 'services_icon'));?>"></i></a> </div>
                        <div class="icon-content">
                            <h5 class="aon-tilte text-uppercase"><?php the_title(); ?></h5>
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                        </div>
                    </div>
                </div>
                <?php $count++; endwhile; ?>
            </div>
        </div>
    </div>
</div>
<!-- Architecture End -->

<?php elseif ( $testi_style == 'style_four' ) : ?>

<!-- Architecture -->
<div class="section-full aon-our-team bg-white content-inner">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
            <span class="title-small"><?php echo wp_kses_post($text); ?></span>
            <div class="after-titile-line"></div>
        </div>
        <div class="section-content">
            <div class="row">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$services_meta = _WSH()->get_meta();
				?>
                <div class="col-md-4 col-sm-6">
                    <div class="icon-bx-wraper bx-style-2 m-l40 m-b30 p-a30 left">
                        <div class="icon-bx-sm radius bg-primary m-b20"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>" class="icon-cell"><i class="fa <?php echo str_replace("icon ", "",bheem_set($services_meta, 'services_icon'));?>"></i></a> </div>
                        <div class="icon-content p-l40">
                            <h5 class="aon-tilte text-uppercase"><?php the_title(); ?></h5>
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Architecture END -->

<?php else : ?>

<!-- Architecture -->
<div class="section-full bg-white content-inner overlay-white-middle" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>'); background-position:right center; background-repeat:no-repeat; background-size: auto 100%;">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <div class="col-md-4 col-sm-6">
                <div class="icon-bx-wraper center m-b40">
                    <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i class="fa <?php echo str_replace("icon ", "",bheem_set($services_meta, 'services_icon'));?> text-primary"></i></span> </div>
                    <div class="icon-content">
                        <h5 class="dez-tilte text-uppercase"><?php the_title(); ?></h5>
                        <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</div>
<!-- Architecture END-->

<?php endif; endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>