<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['projects_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>

<!-- Our Project -->
<div class="section-full bg-img-fix overlay-black-dark dez-our-project content-inner-1" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-white">
                <h2 class="m-t0"><?php echo wp_kses_post($contents); ?></h2>
                <p class="text-black"><?php echo wp_kses_post($text); ?></p>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="service-carousel">
                    <?php while($query->have_posts()): $query->the_post();
						global $post ; 
						$project_meta = _WSH()->get_meta();
					?>
                    <div class="item">
                        <div class="ow-carousel-entry">
                            <div class="ow-entry-media dez-img-effect zoom-slow"> 
                                <a href="<?php echo esc_url(bheem_set($project_meta, 'ext_url')); ?>"><?php the_post_thumbnail('bheem_700x466'); ?></a> 
                            </div>
                            <div class="ow-entry-content">
                                <div class="date bg-primary "><?php echo get_the_date(); ?></div>
                                <div class="ow-entry-title">
                                    <h4 class="text-uppercase"><a href="<?php echo esc_url(bheem_set($project_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h4>
                                </div>
                                <div class="ow-entry-text">
                                    <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;?>
                </div>
            </div>
       </div>
    </div>
</div>
<!-- Our Project END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>