<?php
ob_start() ;?>

<!-- Call To Action -->
<div class="section-full bg-img-fix overlay-primary-dark p-tb50 dez-support" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="m-b15 m-t0 text-black "><?php echo wp_kses_post($title); ?></h2>
                <h3 class="m-t0 m-b20 text-black "><?php echo wp_kses_post($text); ?></h3>
                <a href="<?php echo esc_url($btn_link); ?>" class="site-button black radius-sm"><?php echo wp_kses_post($btn_title); ?></a>
            </div>
       </div>
    </div>
</div>
<!-- Call To Action END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>