<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['projects_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- Projects -->
<div class="section-full content-inner our-project-box overlay-black-dark" style="background-image:url(<?php echo esc_url(wp_get_attachment_url($bg_img));?>);">
    <div class="container">
        <div class="section-head  text-center text-white">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title);?></h2>
            <div class="dez-separator-outer">
                <div class="dez-separator bg-white style-skew"></div>
            </div>
            <p><?php echo wp_kses_post($text);?></p>
        </div>
        <div class="row">
        	<?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$projects_meta = _WSH()->get_meta();
			?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-12 m-b30">
                <div class="dez-box wpb_animate_when_almost_visible wpb_flipInY flipInY">
                    <div class="dez-media dez-img-effect zoom"> <?php the_post_thumbnail('bheem_480x430'); ?>
                        <div class="dez-info-has p-a10 bg-black">
                            <h5 class="m-a0"><?php the_title();?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
		
		<div class="row">
            <div class="col-md-12 m-b30 text-center">
                <a href="https://urdzines.com/projects/" class="site-button m-t10 radius-no">View All Projects</a>
            </div>
        </div>
		
    </div>
</div>

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>