<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>

<!-- Team member -->
<div class="section-full bg-white content-inner">
    <div class="container">
        <div class="section-head text-center ">
            <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content text-center ">
            <div class="row">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$team_meta = _WSH()->get_meta();
				?>
                <div class="col-md-2 col-sm-6">
                    <div class="dez-box m-b30">
                        <div class="dez-media"> <a href="<?php echo esc_url(bheem_set($team_meta, 'ext_url'));?>"> <?php the_post_thumbnail('bheem_500x560'); ?> </a>
                            <?php if($socials = bheem_set($team_meta, 'bunch_team_social')):?>
                            <div class="dez-info-has skew-has  bg-primary">
                                <ul class="dez-social-icon border">
                                    <?php foreach($socials as $key => $value):?>
                                        <li><a href="<?php echo esc_url(bheem_set($value, 'social_link'));?>" class="fa <?php echo esc_attr(bheem_set($value, 'social_icon'));?>"></a></li>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                            <?php endif;?>
                        </div>
                        <div class="p-a10">
                            <h4 class="dez-title text-uppercase"><a href="<?php echo esc_url(bheem_set($team_meta, 'ext_url'));?>"><?php the_title(); ?></a></h4>
                            <span class="dez-member-position"><?php echo wp_kses_post(bheem_set($team_meta, 'designation'));?></span> 
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
			
			<div class="row">
            <div class="col-md-12 m-b30 text-center">
                <a href="https://urdzines.com/meet-our-team/" class="site-button m-t10 radius-no">View All</a>
            </div>
        </div>
			
        </div>
    </div>
</div>
<!-- Team member END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>