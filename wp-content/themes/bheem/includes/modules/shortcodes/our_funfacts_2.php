<?php
ob_start() ;?>

<!-- Company staus -->
<div class="section-full text-white bg-img-fix content-inner overlay-black-middle" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="row">
            <?php $skills_array = (array)json_decode(urldecode($funfact));
				if( $skills_array && is_array($skills_array) ): 
				foreach( (array)$skills_array as $key => $value ):
			?>
            <div class="col-md-3 col-sm-6 m-b30">
                <div class="p-a30 text-white text-center border-3">
                    <div class="icon-lg m-b20"><i class="fa <?php echo esc_attr(bheem_set( $value, 'icons' )); ?>"></i></div>
                    <div class="counter font-26 font-weight-800 text-primary m-b5"><?php echo wp_kses_post(bheem_set( $value, 'counter_value' )); ?></div>
                    <span><?php echo wp_kses_post(bheem_set( $value, 'title' )); ?></span> 
                </div>
            </div>
            <?php endforeach; endif;?>
        </div>
    </div>
</div>
<!-- Company staus END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>