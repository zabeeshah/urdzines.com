<?php
ob_start() ;?>

<!-- Counters -->
<div class="section-full bg-white content-inner overlay-primary-dark text-white bg-img-fix" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>')">
    <div class="container">
        <div class="row">
            <?php $skills_array = (array)json_decode(urldecode($funfact_four));
				if( $skills_array && is_array($skills_array) ): 
				foreach( (array)$skills_array as $key => $value ):
			?>
            <div class="col-md-3 col-sm-6 m-b30">
                <div class="counter-left counter-box">
                    <div class="icon-md pull-left m-tb5"><i class="ti-home"></i></div>
                    <div class="clearfix m-l60">
                        <div class="counter m-b5"><?php echo esc_attr(bheem_set( $value, 'counter_value' )); ?></div>
                        <span class="font-16"><?php echo esc_attr(bheem_set( $value, 'title' )); ?></span> 
                    </div>
                </div>
            </div>
            <?php endforeach; endif;?>
        </div>
    </div>
</div>
<!-- Counters END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>