<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- Services -->
<div class="section-full bg-white content-inner">
    <div class="container">
        <div class="section-head text-center ">
            <h2 class="text-uppercase"><?php echo wp_kses_post($contents); ?></h2>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content ">
            <div class="row">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$services_meta = _WSH()->get_meta();
				?>
                <?php if(($count%3) == 0 && $count != 0):?>
            </div>
            <div class="row">	
                <?php endif; ?>
                <div class="col-md-4 col-sm-4 m-b30">
                    <div class="dez-box">
                        <div class="dez-media"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_post_thumbnail('bheem_723x389'); ?></a> </div>
                        <div class="dez-info p-a20 text-center bg-gray">
                            <div class="p-lr20">
                                <h4 class="m-a0 m-b15"><a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h4>
                                <div class="dez-separator bg-primary"></div>
                            </div>	
                            <p class="m-b0"><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                        </div>
                    </div>
                </div>
                <?php $count++; endwhile; ?>
            </div>
        </div>
    </div>
</div>
<!-- Services END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>