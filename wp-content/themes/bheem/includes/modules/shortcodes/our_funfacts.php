<?php
ob_start() ;?>

<!-- Company staus -->
<div class="section-full text-white bg-img-fix content-inner overlay-black-middle" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="row">
            <?php $skills_array = (array)json_decode(urldecode($funfact));
				if( $skills_array && is_array($skills_array) ): 
				foreach( (array)$skills_array as $key => $value ):
			?>
            <div class="col-md-3 col-sm-6">
                <div class="dex-box text-primary border-2 counter-box m-b30">
                    <h2 class="text-uppercase m-a0 p-a15 "><i class="fa <?php echo esc_attr(bheem_set( $value, 'icons' )); ?> m-r20"></i> <span class="counter"><?php echo wp_kses_post(bheem_set( $value, 'counter_value' )); ?></span></h2>
                    <h5 class="dez-tilte  text-uppercase m-a0"><span class="dez-tilte-inner skew-title bg-primary p-lr15 p-tb10"><?php echo wp_kses_post(bheem_set( $value, 'title' )); ?></span></h5>
                </div>
            </div>
            <?php endforeach; endif;?>
        </div>
    </div>
</div>
<!-- Company staus END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>