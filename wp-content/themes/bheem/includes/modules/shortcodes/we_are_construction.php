<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- Our Awesome Services -->
<div class="section-full">
    <div class="container-fluid">
        <div class="row dzseth">
            <div class="col-md-7 col-sm-8 bg-gray p-lr0 box-services">
                <div class="p-a30 box-services-content">
                    <div class="max-w800 m-auto p-tb30">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-uppercase"><?php echo wp_kses_post($title);?></h2>
                                <div class="dlab-separator bg-primary"></div>
                                <p><?php echo wp_kses_post($text);?></p>
                            </div>
                        </div>
                        <div class="row">
                            <?php while($query->have_posts()): $query->the_post();
								global $post ; 
								$services_meta = _WSH()->get_meta();
							?>
                            <div class="col-md-6 m-b30">
                                <h3 class="m-t0 m-b15"><?php the_title(); ?></h3>
                                <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                                <div class="m-t20"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>" class="site-button"><?php esc_html_e('Read More', 'bheem'); ?></a> </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-4 p-a0"> 
                <img src="<?php echo esc_url(wp_get_attachment_url($image));?>" alt="" class="img-cover"/> 
            </div>
        </div>
    </div>
</div>
<!-- Our Awesome Services END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>