<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- Our Services -->
<div class="section-full content-inner overlay-white-dark" style="background-image:url(<?php echo esc_url(wp_get_attachment_url($bg_img));?>)">
    <div class="container">
        <div class="row">
            
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <div class="col-md-4 col-sm-4 m-b30">
                <div class="dez-box fly-box">
                    <div class="dez-media"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_post_thumbnail('bheem_700x438'); ?></a> </div>
                    <div class="dez-info p-a30 bg-white">
                        <h2 class="dez-title m-t0"><a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_title();?></a></h2>
                        <div class="dez-separator bg-primary"></div>
                        <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                        <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>" class="site-button text-secondry"><?php esc_html_e('Read More', 'bheem'); ?></a>
                    </div>
                </div>
            </div>
            <?php $count++; endwhile; ?>
        </div>
    </div>
</div>

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>