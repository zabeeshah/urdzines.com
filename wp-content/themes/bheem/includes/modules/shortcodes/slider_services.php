<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- Services -->
<div class="section-full bg-img-fix content-inner-1 overlay-black-middle" style="background-image:url(<?php echo esc_url(wp_get_attachment_url($bg_img));?>);">
    <div class="container">
        <div class="section-head text-center text-white">
            <h2 class="text-uppercase"><?php echo wp_kses_post($contents); ?></h2>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content owl-none">
            <div class="img-carousel-content mfp-gallery gallery owl-btn-center-lr">
            	<?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$services_meta = _WSH()->get_meta();
				?>
                <div class="item">
                    <div class="ow-carousel-entry">
                        <div class="ow-entry-media dez-img-effect zoom-slow"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_post_thumbnail('bheem_700x438'); ?></a> </div>
                        <div class="ow-entry-content">
                            <div class="ow-entry-title text-uppercase"><a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_title(); ?></a></div>
                            <div class="ow-entry-text">
                                <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                            </div>
                            <div class="ow-entry-button"></div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>
<!-- Services End -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>