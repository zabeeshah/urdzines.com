<?php
$count=1;
$counts=01;
ob_start() ;?>

<!-- Service -->
<div class="section-full content-inner bg-white">
    <div class="container">
        
        <?php $skills_array = (array)json_decode(urldecode($services_v7));
			if( $skills_array && is_array($skills_array) ): 
			foreach( (array)$skills_array as $key => $value ):
		?>
        <?php if( $count == 2 ):?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-items-center d-flex mb-sm-4 mb-4 mb-md-0">
                <div class="relative service-number">
                    <div class="number"><?php echo esc_attr($counts);?></div>
                    <h2 class="text-capitalize"><?php echo wp_kses_post(bheem_set( $value, 'title' )); ?></h2>
                    <p><?php echo wp_kses_post(bheem_set( $value, 'text' )); ?></p>
                    <ul class="list-check primary">
                    	<?php echo wp_kses_post(bheem_set( $value, 'content' )); ?>
                    </ul>
                    <a href="<?php echo esc_url(bheem_set( $value, 'btn_link' )); ?>" class="site-button"><?php echo wp_kses_post(bheem_set( $value, 'btn_title' )); ?></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="ser-img-eff">
                    <img src="<?php echo esc_url(wp_get_attachment_url(bheem_set( $value, 'img' ))); ?>" alt="<?php esc_attr_e( 'Awesome Image', 'bheem' );?>">
                </div>
            </div>
        </div>
        <?php else:?>
        <div class="row m-b50">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 mb-sm-4 mb-4 mb-md-0">
                <div class="ser-img-eff">
                    <img src="<?php echo esc_url(wp_get_attachment_url(bheem_set( $value, 'img' ))); ?>" alt="<?php esc_attr_e( 'Awesome Image', 'bheem' );?>">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-items-center d-flex">
                <div class="relative service-number">
                    <div class="number"><?php echo esc_attr($counts);?></div>
                    <h2 class="text-capitalize"><?php echo wp_kses_post(bheem_set( $value, 'title' )); ?></h2>
                    <p><?php echo wp_kses_post(bheem_set( $value, 'text' )); ?></p>
                    <ul class="list-check primary">
                    	<?php echo wp_kses_post(bheem_set( $value, 'content' )); ?>
                    </ul>
                    <a href="<?php echo esc_url(bheem_set( $value, 'btn_link' )); ?>" class="site-button"><?php echo wp_kses_post(bheem_set( $value, 'btn_title' )); ?></a>
                </div>
            </div>
        </div>
        <?php endif;?>
        
        <?php $count++; $counts++; endforeach; endif;?>
        
    </div>
</div>

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>