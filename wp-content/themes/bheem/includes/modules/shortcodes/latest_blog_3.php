<?php  
   global $post ;
   $count = 0;
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>  
<?php if($query->have_posts()):  ?>   

<!-- Latest Blog -->
<div class="section-full bg-white content-inner">
    <div class="container">
        <div class="section-head text-center ">
            <h2 class="text-uppercase"><?php echo wp_kses_post($contents); ?></h2>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content ">
            <div class="blog-carousel">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$post_meta = _WSH()->get_meta();
				?>
                <div class="m-b30">
                    <div class="dez-box">
                        <div class="dez-media"> 
                            <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_post_thumbnail('bheem_700x438'); ?></a> 
                        </div>
                        <div class="dez-info p-a20 border-1">
                            <ul class="blog-info text-primary">
                                <li><?php esc_html_e('By', 'bheem'); ?> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>" title="<?php esc_attr_e('Posts by demongo', 'bheem'); ?>" rel="author"><?php the_author(); ?></a> </li>
                                <li><a href="<?php echo esc_url(get_the_permalink(get_the_id()).'#comment');?>" class="comments-link"><?php comments_number( wp_kses_post(__('0 Comments' , 'bheem')), wp_kses_post(__('1 Comment' , 'bheem')), wp_kses_post(__('% Comments' , 'bheem'))); ?></a> </li>
                                <li><span><?php echo get_the_date(); ?></span> </li>
                            </ul>
                            <h4 class="dez-title m-t0"><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h4>
                            <p class="m-a0"><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Latest Blog END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>