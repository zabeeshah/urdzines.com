<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<div class="section-full content-inner bg-white" style="background-image:url(<?php echo esc_url(wp_get_attachment_url($bg_img));?>)">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="title-top"><?php echo wp_kses_post($title);?></h2>
            <h2 class="h1 text-secondry"><?php echo wp_kses_post($sub_title);?></h2>
            <p><?php echo wp_kses_post($text);?></p>
        </div>
        <div class="row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <div class="col-md-3 col-sm-6 m-b30">
                <div class="service-border-style">
                    <?php the_post_thumbnail('bheem_700x500'); ?>
                    <div class="p-a10 services-content">
                        <!--<div class="icon-md text-gray-dark"><i class="ti-user"></i></div>-->
                        <h2 class="dez-title m-t10"><a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_title();?></a></h2>
                        <div class="dez-separator bg-primary"></div>
                        <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
        <div class="row">
            <div class="col-md-12 m-b30 text-center">
                <a href="<?php echo esc_url($btn_link);?>" class="site-button m-t10 radius-no"><?php echo wp_kses_post($btn_text);?></a>
                <!--<a href="<?php echo esc_url($btn_link1);?>" class="site-button m-t10 m-l10 radius-no bg-secondry"><?php echo wp_kses_post($btn_text1);?></a>-->
            </div>
        </div>
    </div>
</div>	

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>