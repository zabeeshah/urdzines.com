<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>   
<?php if($query->have_posts()):  ?>

<?php if ( $testi_style == 'style_two' ) : ?>

<!-- Testimonials blog -->
<div class="section-full overlay-black-middle bg-img-fix content-inner-1" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="section-head text-white text-center">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-white  style-skew"></div>
            </div>
        </div>
        <div class="section-content">
            <div class="testimonial-one">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$testimonial_meta = _WSH()->get_meta();
				?>
                <div class="item">
                    <div class="testimonial-1 testimonial-bg">
                        <div class="testimonial-pic quote-left radius shadow"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                        <div class="testimonial-text">
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?></p>
                        </div>
                        <div class="testimonial-detail"> <strong class="testimonial-name"><?php the_title(); ?></strong> <span class="testimonial-position"><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></span> </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Testimonials blog END -->

<?php elseif ( $testi_style == 'style_three' ) : ?>

<!-- Testimoniyal -->
<div class="section-full bg-img-fix content-inner overlay-white-dark" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>'); margin-top:-1px">
    <div class="container">
        <div class="section-head text-center ">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content">
            <div class="testimonial-five">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$testimonial_meta = _WSH()->get_meta();
				?>
                <div class="item">
                    <div class="testimonial-6">
                        <div class="testimonial-text bg-white quote-left quote-right">
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?> </p>
                        </div>
                        <div class="testimonial-detail clearfix bg-primary text-white">
                            <h4 class="testimonial-name m-tb0"><?php the_title(); ?></h4>
                            <span class="testimonial-position"><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></span>
                            <div class="testimonial-pic radius"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Testimoniyal End -->

<?php elseif ( $testi_style == 'style_four' ) : ?>

<!-- What peolpe are saying -->
<div class="section-full overlay-black-middle bg-img-fix content-inner-1" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="section-head text-center text-white">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <span class="title-small"><?php echo wp_kses_post($text); ?></span>
            <div class="after-titile-line"></div>
        </div>
        <div class="section-content">
            <div class="testimonial-one">
                <?php while($query->have_posts()): $query->the_post();
                    global $post ; 
                    $testimonial_meta = _WSH()->get_meta();
                ?>
                <div class="item">
                    <div class="testimonial-1 testimonial-bg">
                        <div class="testimonial-pic quote-left radius shadow"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                        <div class="testimonial-text">
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?></p>
                        </div>
                        <div class="testimonial-detail"> <strong class="testimonial-name"><?php the_title(); ?></strong> <span class="testimonial-position"><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></span> </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- What peolpe are saying END -->

<?php elseif ( $testi_style == 'style_five' ) : ?>

<!-- What peolpe are saying -->
<div class="section-full overlay-black-middle bg-img-fix content-inner-1" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="section-head text-white text-center">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <span class="title-small"><?php echo wp_kses_post($text); ?></span>
            <div class="after-titile-line"></div>
        </div>
        <div class="section-content">
            <div class="testimonial-four">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$testimonial_meta = _WSH()->get_meta();
				?>
                <div class="item">
                    <div class="testimonial-4 style-2">
                        <div class="testimonial-pic"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                        <div class="testimonial-text">
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?></p>
                        </div>
                        <div class="testimonial-detail"> <strong class="testimonial-name"><?php the_title(); ?></strong> <span class="testimonial-position"><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></span> </div>
                        <div class="quote-right"></div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- What peolpe are saying END-->


<?php else : ?>

<!-- Testimonials blog -->
<div class="section-full overlay-black-middle bg-img-fix content-inner-1" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="section-head text-white text-center">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-white  style-skew"></div>
            </div>
        </div>
        <div class="section-content">
            <div class="testimonial-four">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$testimonial_meta = _WSH()->get_meta();
				?>
                <div class="item">
                    <div class="testimonial-4 testimonial-bg">
                        <div class="testimonial-pic"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                        <div class="testimonial-text">
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?></p>
                        </div>
                        <div class="testimonial-detail"> <strong class="testimonial-name"><?php the_title(); ?></strong> <span class="testimonial-position"><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></span> </div>
                        <div class="quote-right"></div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Testimonials blog END -->

<?php endif; endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>