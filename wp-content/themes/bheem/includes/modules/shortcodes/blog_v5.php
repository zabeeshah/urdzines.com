<?php  
   global $post ;
   $count = 0;
   $paged = get_query_var('paged');
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order, 'paged'=>$paged);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>

<div class="section-full content-inner bg-white">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="title-top"><?php echo wp_kses_post($title);?></h2>
            <h2 class="h1 text-secondry"><?php echo wp_kses_post($sub_title);?></h2>
            <p><?php echo wp_kses_post($text);?></p>
        </div>
        <div class="blog-carousel owl-btn-center-lr">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$post_meta = _WSH()->get_meta();
			?>
            <div class="item">
                <div class="blog-post-style">
                    <?php the_post_thumbnail('bheem_700x500'); ?>
                    <h2 class="m-b5"><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_title();?></a></h2>
                    <ul class="list-inline">
                        <li class="post-date"> <i class="ti-calendar m-r10"></i><span><?php echo get_the_date();?></span> </li>
                        <li class="post-author"><i class="ti-user m-r10"></i><?php esc_html_e('By', 'bheem');?> <a class="text-secondry" href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a> </li>
                    </ul>
                    <div class="usefull-link">
                        <?php echo wp_kses_post( bheem_category_class($thelist) );?>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</div>

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>