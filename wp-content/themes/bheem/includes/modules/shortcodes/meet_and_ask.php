<?php
ob_start() ;?>

<!-- meet & ask -->
<div class="section-full z-index100 meet-ask-outer">
    <div class="container">
        <div class="row">
            <div class="col-md-9 meet-ask-row p-tb30">
                <div class="row">
                    <div class="col-md-6">
                        <div class="icon-bx-wraper clearfix text-white left">
                            <div class="icon-xl "> <span class=" icon-cell"><i class="fa fa-building-o"></i></span> </div>
                            <div class="icon-content">
                                <h3 class="dez-tilte text-uppercase m-b10"><?php echo wp_kses_post($title); ?></h3>
                                <p><?php echo wp_kses_post($text); ?> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 m-t20 ">
						<div class="wpb_animate_when_almost_visible wpb_bounceInRight bounceInRight">
							<a href="<?php echo esc_url($btn_link); ?>" class="site-button-secondry button-skew m-l10 "><span><?php echo wp_kses_post($btn_title); ?></span><i class="fa fa-angle-right"></i></a>
						</div>   
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- meet & ask END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>