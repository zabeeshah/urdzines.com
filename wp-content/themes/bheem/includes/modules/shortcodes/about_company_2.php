<?php
ob_start() ;?>

<!-- About Company -->
<div class="section-full  bg-gray content-inner-1" style="background-image: url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>'); background-repeat: repeat-x; background-position: left bottom -37px;">
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-5 m-b30">
                    <div class="dez-thu m"><img src="<?php echo esc_url(wp_get_attachment_url($image)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></div>
                </div>
                <div class="col-md-7">
                    <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
                    <div class="dez-separator-outer ">
                        <div class="dez-separator bg-secondry style-skew"></div>
                    </div>
                    <div class="clear"></div>
                    <p><strong><?php echo wp_kses_post($text); ?></strong></p>
                    <p class="m-b30"><?php echo wp_kses_post($text1); ?></p>
                    <div class="row">
                        <?php $skills_array = (array)json_decode(urldecode($services));
							if( $skills_array && is_array($skills_array) ): 
							foreach( (array)$skills_array as $key => $value ):
						?>
                        <div class="col-md-6 col-sm-6">
                            <div class="icon-bx-wraper bx-style-1 p-a20 left m-b30">
                                <div class="bg-secondry icon-bx-xs m-b20 "> <span class="icon-cell"><i class="fa <?php echo esc_attr(bheem_set( $value, 'icons' )); ?> text-primary"></i></span> </div>
                                <div class="icon-content">
                                    <h5 class="dez-tilte text-uppercase"><?php echo wp_kses_post(bheem_set( $value, 'ser_title' )); ?></h5>
                                    <p><?php echo wp_kses_post(bheem_set( $value, 'ser_text' )); ?></p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Company END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>