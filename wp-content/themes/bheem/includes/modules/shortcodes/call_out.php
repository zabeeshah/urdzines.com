<?php
ob_start() ;?>

<!-- About Section -->
<div class="section-full bg-img-fix p-tb40 overlay-primary-dark get-a-quote" style="background-image:url(<?php echo esc_url(wp_get_attachment_url($bg_img));?>);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="pull-left m-b0"><?php echo wp_kses_post($text);?></h4>
                <div class="pull-right"><a href="<?php echo esc_url($btn_link);?>" class="site-button black radius-sm"><?php echo wp_kses_post($btn_title);?></a></div>
            </div>
        </div>
    </div>
</div>	

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>