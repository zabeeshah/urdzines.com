<?php  
   global $post ;
   $count = 0;
   $paged = get_query_var('paged');
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order, 'paged'=>$paged);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>

<!-- Content -->
<div class="page-content">

    <div class="content-area">
        <div class="container">
            <div class="row">
                <!-- Left part start -->
                <div class="col-md-9">
                    <?php while($query->have_posts()): $query->the_post();
                        global $post ; 
                        $post_meta = _WSH()->get_meta();
                    ?>
                    <div class="blog-post blog-md clearfix date-style-2">
                        <div class="dez-post-media dez-img-effect zoom-slow"> <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_post_thumbnail('bheem_700x500'); ?></a> </div>
                        <div class="dez-post-info">
                            <div class="dez-post-title ">
                                <h3 class="post-title"><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
                            </div>
                            <div class="dez-post-meta ">
                                <ul>
                                    <li class="post-date"> <i class="fa fa-calendar"></i><strong><?php echo get_the_date(); ?></strong> </li>
                                    <li class="post-author"><i class="fa fa-user"></i><?php esc_html_e('By', 'bheem'); ?> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a> </li>
                                    <li class="post-comment"><i class="fa fa-comments"></i> <a href="<?php echo esc_url(get_the_permalink(get_the_id()).'#comment');?>"><?php comments_number( '0', '1', '%' ); ?></a> </li>
                                </ul>
                            </div>
                            <div class="dez-post-text">
                                <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                            </div>
                            <div class="dez-post-readmore"> <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>" title="<?php esc_attr_e('READ MORE', 'bheem'); ?>" rel="bookmark" class="site-button-link"><?php esc_html_e('READ MORE', 'bheem'); ?>&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i></a> </div>
                            <div class="dez-post-tags">
                                <div class="post-tags"> <?php the_tags('', '');?> </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;?>
                    <!-- Pagination start -->
                    <div class="pagination-bx clearfix ">
                        <?php bheem_the_pagination(array('total'=>$query->max_num_pages, 'next_text' => '<i class="fa fa-angle-double-right"></i>', 'prev_text' => '<i class="fa fa-angle-double-left"></i>')); ?>
                    </div>
                    <!-- Pagination END -->
                </div>
                <!-- Left part END -->
                
                <!-- Side bar start -->
                <div class="col-md-3">
                    <aside  class="side-bar">
                        <?php if ( is_active_sidebar( $sidebar_slug ) ) : ?>
                            <?php dynamic_sidebar( $sidebar_slug ); ?>
                        <?php endif; ?>
                    </aside>
                </div>
                <!-- Side bar END -->
            </div>
        </div>
    </div>
</div>
<!-- Content END-->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>