<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>   
<?php if($query->have_posts()):  ?>  

<!-- Testimoniyal -->
<div class="section-full bg-img-fix content-inner overlay-white-dark" style="background-image:url(<?php echo esc_url(wp_get_attachment_url($bg_img));?>);">
    <div class="container">
        <div class="section-head text-center ">
        	<h2 class="text-uppercase"><?php echo wp_kses_post($contents); ?></h2>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content">
            <div class="testimonial-four">
                
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$testimonial_meta = _WSH()->get_meta();
				?>
                
                <div class="item">
                    <div class="testimonial-4 testimonial-bg">
                        <div class="testimonial-pic"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                        <div class="testimonial-text">
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?></p>
                        </div>
                        <div class="testimonial-detail"> <strong class="testimonial-name"><?php the_title(); ?></strong> <span class="testimonial-position"><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></span> </div>
                        <div class="quote-right"></div>
                    </div>
                </div>
                
                <?php endwhile;?>
                
            </div>
        </div>
    </div>
</div>
<!-- Testimoniyal End -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>