<?php
ob_start() ;?>

<!-- Counter -->
<div class="section-full bg-gray bg-img-fix content-inner overlay-black-middle" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="row">
            <?php $skills_array = (array)json_decode(urldecode($funfact));
				if( $skills_array && is_array($skills_array) ): 
				foreach( (array)$skills_array as $key => $value ):
			?>
            <div class="col-md-3 col-sm-3">
                <div class="m-b30 text-white text-center">
                    <div class="icon-bx-lg radius  border-2 m-b20">
                        <div class="icon-cell text-white"> <i class="fa <?php echo esc_attr(bheem_set( $value, 'icons' )); ?>"></i> </div>
                    </div>
                    <div class="counter font-26 font-weight-800 text-primary m-b5"><?php echo wp_kses_post(bheem_set( $value, 'counter_value' )); ?></div>
                    <div class="aon-separator bg-primary"></div>
                    <p><?php echo wp_kses_post(bheem_set( $value, 'title' )); ?></p>
                </div>
            </div>
            <?php endforeach; endif;?>
        </div>
    </div>
</div>
<!-- Counter End -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>