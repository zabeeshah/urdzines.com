<?php
ob_start() ;?>

<!-- About Company -->
<div class="section-full  bg-gray content-inner-1" style="background-image: url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>'); background-repeat: repeat-x; background-position: left bottom -37px;">
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-7">
                    <h2 class="text-uppercase"> <?php echo wp_kses_post($title); ?></h2>
                    <div class="dez-separator-outer ">
                        <div class="dez-separator bg-secondry style-skew"></div>
                    </div>
                    <div class="clear"></div>
                    <p><strong><?php echo wp_kses_post($text); ?></strong></p>
                    <p class="m-b50"><?php echo wp_kses_post($text1); ?></p>
                    <div class="row">
                        <?php $skills_array = (array)json_decode(urldecode($services));
							$icons_arry = array("A", "I", "C","F");
							$my_itr = 0;
							if( $skills_array && is_array($skills_array) ): 
							foreach( (array)$skills_array as $key => $value ):
						?>
                        <div class="col-md-6 col-sm-6 wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight">
                            <div class="icon-bx-wraper left m-b30">
                                <div class="icon-bx-sm bg-secondry "> <span class="icon-cell"><i class="fa <?php //echo esc_attr(bheem_set( $value, 'icons' )); ?> text-primary" style="font-size:60px;"><?php echo $icons_arry[$my_itr]; ?></i></span> </div>
                                <div class="icon-content">
                                    <h3 class="dez-tilte text-uppercase"><?php echo wp_kses_post(bheem_set( $value, 'ser_title' )); ?></h3>
                                    <p><?php echo wp_kses_post(bheem_set( $value, 'ser_text' )); ?></p>
                                </div>
                            </div>
                        </div>
                        <?php 
							$my_itr++;
						endforeach; endif;?>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="dez-thu m wpb_animate_when_almost_visible wpb_flipInY flipInY"><img src="<?php echo esc_url(wp_get_attachment_url($image)); ?>" alt="<?php esc_attr_e('Awesome Image', 'bheem'); ?>"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Company END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>