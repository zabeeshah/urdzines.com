<?php
ob_start() ;?>

<!-- Content -->
<div class="page-content bg-white">

	<!-- contact area -->
    <div class="section-full content-inner contact-v2 contact-style-1">
        <div class="container">
            <div class="row">
                <?php $skills_array = (array)json_decode(urldecode($service));
					if( $skills_array && is_array($skills_array) ): 
					foreach( (array)$skills_array as $key => $value ):
				?>
                <div class="col-md-4 col-sm-6 m-b30">
                    <div class="icon-bx-wraper bx-style-1 p-a30 center">
                        <div class="icon-xl text-primary m-b20"> <a href="<?php echo esc_url(bheem_set( $value, 'link' )); ?>" class="icon-cell"><i class="fa <?php echo esc_attr(bheem_set( $value, 'icons' )); ?>"></i></a> </div>
                        <div class="icon-content">
                            <h5 class="dez-tilte text-uppercase"><?php echo wp_kses_post(bheem_set( $value, 'title1' )); ?></h5>
                            <p><?php echo wp_kses_post(bheem_set( $value, 'text1' )); ?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach; endif;?>
            </div>
            <div class="row">
                <!-- Left part start -->
                <div class="col-md-6">
                    <div class="p-a30 bg-gray clearfix m-b30 ">
                        <h2><?php echo wp_kses_post($form_title); ?></h2>
                        <div class="dzFormMsg"></div>
                        <?php echo do_shortcode(bunch_base_decode($contact_form));?>
                    </div>
                </div>
                <!-- Left part END -->
                <!-- right part start -->
                <div class="col-md-6">
                    <div class="map-section">
                        <!--Map Outer-->
                        <div class="map-outer">
                            <!--Map Canvas-->
                            <div class="map-canvas"
                                data-zoom="<?php echo esc_js($map_zoom); ?>"
                                data-lat="<?php echo esc_js($lat); ?>"
                                data-lng="<?php echo esc_js($long); ?>"
                                data-type="roadmap"
                                data-hue="#ffc400"
                                data-title="<?php echo esc_js($mark_title); ?>"
                                data-icon-path="<?php echo esc_js(wp_get_attachment_url($img)); ?>"
                                data-content="<?php echo esc_js($address); ?><br><a href='mailto:<?php echo esc_js(sanitize_email($mark_email)); ?>'><?php echo esc_js(sanitize_email($mark_email)); ?></a>">
                            </div>
                        </div>
               		</div>
                </div>
                <!-- right part END -->
            </div>
        </div>
    </div>
    <!-- contact area  END -->
</div>

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>