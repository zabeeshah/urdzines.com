<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>   
<?php if($query->have_posts()):  ?>  

<!-- Team member -->
<div class="section-full bg-white content-inner">
    <div class="container">
        <div class="section-content">
            <div class="row" id="masonry">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$testimonial_meta = _WSH()->get_meta();
				?>
                <div class="col-md-4 col-sm-6 card-container">
                    <div class="testimonial-2 m-b30">
                        <div class="testimonial-text">
                            <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?></p>
                        </div>
                        <div class="testimonial-detail clearfix">
                            <div class="testimonial-pic quote-left radius shadow"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                            <strong class="testimonial-name"><?php the_title(); ?></strong> <span class="testimonial-position"><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></span> 
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Team member END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>