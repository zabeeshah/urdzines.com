<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['projects_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>


<!-- contact area -->
<div class="section-full bg-white content-inner">
    <!-- About Company -->
    <div class="container">
        <div class="row m-b30">
            <div class="col-md-9 col-sm-8">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="dez-box">
                            <div class="dez-media"> <a href="<?php echo esc_url($link); ?>"><img src="<?php echo esc_url(wp_get_attachment_url($proj_img1)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a> </div>
                            <div class="dez-info m-t30 ">
                                <h4 class="dez-title m-t0"><a href="<?php echo esc_url($link); ?>"><?php echo wp_kses_post($title); ?> </a></h4>
                                <?php echo wp_kses_post($proj_des); ?>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="dez-box">
                            <div class="dez-media m-b30 p-b5"> <a href="<?php echo esc_url($link); ?>"><img src="<?php echo esc_url(wp_get_attachment_url($proj_img2)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a></div>
                            <div class="dez-media"> <a href="<?php echo esc_url($link); ?>"><img src="<?php echo esc_url(wp_get_attachment_url($proj_img3)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a></div>
                            <div class="dez-info m-t30 ">
                                <h4 class="dez-title m-t0"><a href="<?php echo esc_url($link); ?>"><?php echo wp_kses_post($sub_title); ?></a></h4>
                                <?php echo wp_kses_post($text); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 side-sticky">
                <?php if ( is_active_sidebar( $sidebar_slug ) ) : ?>
					<?php dynamic_sidebar( $sidebar_slug ); ?>
                <?php endif; ?>
            </div>
        
        </div>
        
        <?php if($query->have_posts()):  ?>
            <div class="section-content">
                <div class="portfolio-carousel mfp-gallery gallery owl-btn-center-lr">
                    <?php while($query->have_posts()): $query->the_post();
                        global $post ; 
                        $project_meta = _WSH()->get_meta();
                    ?>
                    <?php 
						$post_thumbnail_id = get_post_thumbnail_id($post->ID);
						$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
					?>
                    <div class="item">
                        <div class="ow-portfolio">
                            <div class="ow-portfolio-img dez-img-overlay1 dez-img-effect zoom-slow"> <?php the_post_thumbnail('bheem_700x438'); ?>
                                <div class="overlay-bx">
                                    <div class="overlay-icon"> <a href="<?php echo esc_url($post_thumbnail_url);?>"  class="mfp-link"> <i class="fa fa-search-plus icon-bx-xs"></i> </a> <a href="<?php echo esc_url(bheem_set($project_meta, 'ext_url')); ?>"  class="mfp-link"> <i class="fa fa-link icon-bx-xs"></i> </a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <!-- About Company END -->
</div>
<!-- contact area  END -->

<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>