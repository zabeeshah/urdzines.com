<?php
ob_start() ;?>

<div class="section-full bg-white content-inner-1 overlay-white-dark"  style="background-image:url(<?php echo esc_url( wp_get_attachment_url($bg_img) );?>)">
    <div class="container">
        <div class="section-content">
            <div class="row m-b40">
                <div class="col-lg-6 col-md-12 align-self-center">
                    <div class="m-b30">
                        <h2 class="m-b20 m-t0"><?php echo wp_kses_post( $title );?></h2>
                        <div class="separator bg-primary w5"></div>
                        <p class="m-b10"><?php echo wp_kses_post( $text );?></p>
                        <p><?php echo wp_kses_post( $content );?></p>
                        <a href="<?php echo esc_url( $btn_link );?>" class="site-button m-r10"> <?php echo wp_kses_post( $btn_title );?> </a>
                        <a href="<?php echo esc_url( $btn_link1 );?>" class="site-button"><?php echo wp_kses_post( $btn_title1 );?></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 m-b30">
                    <img class="img-full" src="<?php echo esc_url( wp_get_attachment_url($img) );?>" alt="<?php esc_attr( 'Awesome Image', 'bheem' );?>"/>
                </div>
            </div>
            <div class="row">
            	<?php 
					$count = 01;
					$skills_array = (array)json_decode(urldecode($best_services));
					if( $skills_array && is_array($skills_array) ): 
					foreach( (array)$skills_array as $key => $value ):
				?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12 m-b30">
                    <div class="box-number">
                        <div class="number">
                            <?php echo esc_attr( $count );?>
                        </div>
                        <h3><?php echo wp_kses_post(bheem_set( $value, 'b_title' )); ?></h3>
                        <p><?php echo wp_kses_post(bheem_set( $value, 'b_text' )); ?></p>
                    </div>
                </div>
                <?php $count++; endforeach; endif;?>
            </div>
        </div>
    </div>
</div>

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>