<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>

<!-- Meet Our Team -->
<div class="section-full bg-white content-inner">
    <div class="container">
        <div class="section-head text-center ">
        	<h2 class="text-uppercase"><?php echo wp_kses_post($contents); ?></h2>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content ">
            <div class="row">
                
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$team_meta = _WSH()->get_meta();
				?>
                
                <div class="col-md-3 col-sm-6 dez-team">
                    <div class="dez-box m-b30">
                        <div class="dez-media dez-media-left dez-img-overlay6 dez-img-effect "> 
                            <a href="<?php echo esc_url(bheem_set($team_meta, 'ext_url'));?>"> <?php the_post_thumbnail('bheem_500x600'); ?> </a>
                            
                            <?php if($socials = bheem_set($team_meta, 'bunch_team_social')):?>
                                <div class="dez-info-has">
                                    <ul class="dez-social-icon border">
                                        <?php foreach($socials as $key => $value):?>
                                            <li><a href="<?php echo esc_url(bheem_set($value, 'social_link'));?>" class="fa <?php echo esc_attr(bheem_set($value, 'social_icon'));?> bg-primary"></a></li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                            <?php endif;?>
                            
                            <div class="p-a10 bg-primary text-center">
                                <h4 class="dez-title text-uppercase m-a5"><a href="<?php echo esc_url(bheem_set($team_meta, 'ext_url'));?>"><?php the_title(); ?></a></h4>
                                <span class="dez-member-position"><?php echo wp_kses_post(bheem_set($team_meta, 'designation'));?></span> 
                            </div>
                    	</div>
                	</div>
            	</div>
                
				<?php endwhile;?>
                
            </div>
        </div>
    </div>
</div>
<!-- Meet Our Team END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>