<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['projects_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>

<!-- Our Projects  -->
<div class="section-full bg-img-fix content-inner-1 overlay-black-middle" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">
        <div class="section-head  text-center text-white">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-white style-skew"></div>
            </div>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="portfolio-carousel mfp-gallery-with-owl gallery owl-btn-center-lr">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$project_meta = _WSH()->get_meta();
			?>
            <?php 
				$post_thumbnail_id = get_post_thumbnail_id($post->ID);
				$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
		    ?>
            <div class="item">
                <div class="ow-portfolio">
                    <div class="ow-portfolio-img dez-img-overlay1 dez-img-effect zoom-slow"> <?php the_post_thumbnail('bheem_700x438'); ?>
                        <div class="overlay-bx">
                            <div class="overlay-icon"> <a href="<?php echo esc_url($post_thumbnail_url);?>"  class="mfp-link" title="<?php the_title_attribute(); ?>"> <i class="fa fa-search-plus icon-bx-xs"></i> </a> <a href="<?php echo esc_url(bheem_set($project_meta, 'ext_url')); ?>" class="mfp-link" title="<?php the_title_attribute(); ?>"> <i class="fa fa-link icon-bx-xs"></i> </a> </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</div>
<!-- Our Projects END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>