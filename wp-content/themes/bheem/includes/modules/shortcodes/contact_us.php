<?php
$options = _WSH()->option();
ob_start() ;?>

<!-- contact area -->
<div class="section-full content-inner contact-v1 bg-white contact-style-1">
    <div class="container">
        <div class="row">
            <!-- Left part start -->
            <div class="col-md-8">
                <div class="p-a30 bg-gray clearfix m-b30 ">
                    <h2><?php echo wp_kses_post($form_title); ?></h2>
                    <div class="dzFormMsg"></div>
                    <?php echo do_shortcode(bunch_base_decode($contact_form));?>
                </div>
            </div>
            <!-- Left part END -->
            <!-- right part start -->
            <div class="col-md-4">
                <div class="p-a30 m-b30 border-1 contact-area">
                    <h2 class="m-b10"><?php echo wp_kses_post($title); ?></h2>
                    <p><?php echo wp_kses_post($text); ?></p>
                    <ul class="no-margin">
                        <?php $count = 0; $skills_array = (array)json_decode(urldecode($service));
							if( $skills_array && is_array($skills_array) ): 
							foreach( (array)$skills_array as $key => $value ):
						?>
                        <li class="icon-bx-wraper left m-b30">
                            <div class="icon-bx-xs bg-primary"> <a href="<?php echo esc_url(bheem_set( $value, 'link' )); ?>" class="icon-cell"><i class="fa <?php echo esc_attr(bheem_set( $value, 'icons' )); ?>"></i></a> </div>
                            <div class="icon-content">
                                <h6 class="text-uppercase m-tb0 dez-tilte"><?php echo wp_kses_post(bheem_set( $value, 'title1' )); ?></h6>
                                <p><?php echo wp_kses_post(bheem_set( $value, 'text1' )); ?></p>
                            </div>
                        </li>
                        <?php endforeach; endif;?>
                    </ul>
                    
                    <?php if($show_social): ?>
						
						<?php if(bheem_set( $options, 'social_media' ) && is_array( bheem_set( $options, 'social_media' ) )): ?>
                        <div class="m-t20">
                            <ul class="dez-social-icon border dez-social-icon-lg">
                                <?php $social_icons = bheem_set( $options, 'social_media' );
                                    foreach( bheem_set( $social_icons, 'social_media' ) as $social_icon ):
                                    if( isset( $social_icon['tocopy' ] ) ) continue; ?>
                                    <li><a href="<?php echo esc_url(bheem_set( $social_icon, 'social_link')); ?>" class="fa <?php echo esc_attr(bheem_set( $social_icon, 'social_icon')); ?> bg-primary"></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php endif;?>
                        
               		<?php endif; ?>
                </div>
            </div>
            <!-- right part END -->
        </div>
        
    </div>
</div>
<!-- contact area  END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>