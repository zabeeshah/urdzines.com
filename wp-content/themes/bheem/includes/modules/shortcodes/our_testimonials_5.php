<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>   
<?php if($query->have_posts()):  ?>

<div class="section-full content-inner-1 overlay-white-dark" style="background-image: url(<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>);">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="title-top"><?php echo wp_kses_post($title); ?></h2>
            <h2 class="h1 text-secondry"><?php echo wp_kses_post($sub_title); ?></h2>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="testimonial-one owl-btn-center-lr">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$testimonial_meta = _WSH()->get_meta();
			?>
            <div class="item">
                <div class="testimonial-8 text-secondry text-center max-w700 m-auto">
                    <div class="quote-left"></div>
                    <p class="font-16 m-t50"><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit));?></p>
                    <div class="testimonial-pic"><?php the_post_thumbnail('bheem_100x100'); ?></div>
                    <p class="font-16"><?php the_title();?> - <i><?php echo wp_kses_post(bheem_set($testimonial_meta, 'designation'));?></i></p>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</div>

<?php endif;?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>