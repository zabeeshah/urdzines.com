<?php
ob_start() ;?>

<!-- Client logo -->
<div class="section-full dez-we-find bg-img-fix p-t50 p-b50 ">
    <div class="container">
        <div class="section-content">
            <div class="client-logo-carousel mfp-gallery gallery owl-btn-center-lr">
                <?php $skills_array = (array)json_decode(urldecode($partner));
					if( $skills_array && is_array($skills_array) ): 
					foreach( (array)$skills_array as $key => $value ):
				?>
                <div class="item">
                    <div class="ow-client-logo">
                        <div class="client-logo"><a href="<?php echo esc_url(bheem_set( $value, 'link' )); ?>"><img src="<?php echo esc_url(wp_get_attachment_url(bheem_set( $value, 'image' ))); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a></div>
                    </div>
                </div>
                <?php endforeach; endif;?>
            </div>
        </div>
    </div>
</div>
<!-- Client logo END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>