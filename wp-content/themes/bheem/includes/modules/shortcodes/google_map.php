<?php
ob_start() ;?>

<div class="map-section contact_style_one bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <!-- Map part start -->
            <h2><?php echo wp_kses_post($title); ?></h2>
            
            <!--Map Outer-->
            <div class="map-outer">
                <!--Map Canvas-->
                <div class="map-canvas"
                    data-zoom="<?php echo esc_js($map_zoom); ?>"
                    data-lat="<?php echo esc_js($lat); ?>"
                    data-lng="<?php echo esc_js($long); ?>"
                    data-type="roadmap"
                    data-hue="#ffc400"
                    data-title="<?php echo esc_js($mark_title); ?>"
                    data-icon-path="<?php echo esc_js(wp_get_attachment_url($img)); ?>"
                    data-content="<?php echo esc_js($address); ?><br><a href='mailto:<?php echo esc_js(sanitize_email($mark_email)); ?>'><?php echo esc_js(sanitize_email($mark_email)); ?></a>">
                </div>
            </div>
            
            <!-- Map part END -->
            </div>
        </div>
	</div>
</div>

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>