<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
   <?php if($query->have_posts()):  ?>

<!-- team -->
<div class="section-full content-inner team-box overlay-white-dark" style="background-image: url(<?php echo esc_url( wp_get_attachment_url($bg_img ));?>);">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="text-uppercase"><?php echo wp_kses_post( $title );?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-black style-skew"></div>
            </div>
            <p><?php echo wp_kses_post( $text);?></p>
        </div>
        <div class="row text-center"> 
            
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$team_meta = _WSH()->get_meta();
			?>
            <?php if(($count%4) == 0 && $count != 0):?>
                </div><div class="col-lg-1-5 col-md-12"> </div><div class="col-lg-9"> <div class="row text-center">
            <?php endif; ?>
            
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="dez-box">
                    <div class="dez-media dez-img-effect on-color "><?php the_post_thumbnail('bheem_500x500');?></div>
                    <div class="dez-title-bx">
                        <h3 class="m-b5"><?php the_title(); ?></h3>
                        <p class="m-b0"><?php echo wp_kses_post(bheem_set( $team_meta, 'designation' )); ?></p>
                    </div>
                </div>
            </div>
            <?php $count++; endwhile;?>
            </div>
            <div class="col-lg-1-5 col-md-12"> </div>
        </div>
        
    </div>
</div>

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>