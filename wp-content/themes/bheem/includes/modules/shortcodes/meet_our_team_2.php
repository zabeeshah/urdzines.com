<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>

<!-- Team member -->
<div class="section-full bg-white content-inner">
    <div class="container">
        <div class="section-head text-center ">
            <h2 class="text-uppercase"> <?php echo wp_kses_post($contents); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
            <p><?php echo wp_kses_post($text); ?></p>
        </div>
        <div class="section-content">
            <div class="row">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$team_meta = _WSH()->get_meta();
					if ( $count == 3 ) {
						$count = 0;	
					}
					if($count == 0){
						$class = 'left';
					}
					elseif( $count == 1 ) {
						$class = '';
					}
					else {
						$class = 'right';	
					}
				?>
                <div class="col-md-4 col-sm-4 dez-team-1 <?php echo esc_attr($class); ?>">
                    <div class="dez-box m-b30 team-skew ">
                        <div class="dez-media"> <a href="<?php echo esc_url(bheem_set($team_meta, 'ext_url'));?>"> <?php the_post_thumbnail('bheem_500x730'); ?> </a>
                            <div class="dez-info-has">
                                <?php if($socials = bheem_set($team_meta, 'bunch_team_social')):?>
                                <ul class="dez-social-icon bg-primary">
                                    <?php foreach($socials as $key => $value):?>
                                        <li><a href="<?php echo esc_url(bheem_set($value, 'social_link'));?>" class="fa <?php echo esc_attr(bheem_set($value, 'social_icon'));?>"></a></li>
                                    <?php endforeach;?>
                                </ul>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="p-a20 bg-secondry text-center text-white team-info ">
                            <h4 class="dez-title text-uppercase m-t0 m-b5"><a href="<?php echo esc_url(bheem_set($team_meta, 'ext_url'));?>" class=" text-white"><?php the_title(); ?></a></h4>
                            <span class="dez-member-position"><?php echo wp_kses_post(bheem_set($team_meta, 'designation'));?></span> 
                        </div>
                    </div>
                </div>
                <?php $count++; endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Team member END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>