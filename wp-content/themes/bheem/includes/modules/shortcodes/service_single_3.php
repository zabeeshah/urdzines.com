<?php
ob_start() ;?>

<!-- contact area -->
<div class="section-full bg-white content-inner">
    <!-- About Company -->
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-5">
                <?php if ( is_active_sidebar( $sidebar_slug ) ) : ?>
					<?php dynamic_sidebar( $sidebar_slug ); ?>
                <?php endif; ?>
            </div>
            <div class="col-md-9 col-sm-7">
                <div class="row">
                    
                    <div class="col-md-6 col-sm-12">
                        <div class="dez-box">
                            <div class="dez-media m-b30 p-b5"> <a href="<?php echo esc_url($link); ?>"><img src="<?php echo esc_url(wp_get_attachment_url($ser_img1)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a></div>
                            <div class="dez-media"> <a href="<?php echo esc_url($link); ?>"><img src="<?php echo esc_url(wp_get_attachment_url($ser_img2)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a></div>
                            <div class="dez-info m-t30 ">
                                <h4 class="dez-title m-t0"><a href="<?php echo esc_url($link); ?>"><?php echo wp_kses_post($title); ?></a></h4>
                                <?php echo wp_kses_post($ser_des); ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-sm-12">
                        <div class="dez-box">
                            <div class="dez-media"> <a href="<?php echo esc_url($link); ?>"><img src="<?php echo esc_url(wp_get_attachment_url($ser_img3)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a> </div>
                            <div class="dez-info m-t30">
                                <h4 class="dez-title m-t0"><a href="<?php echo esc_url($link); ?>"><?php echo wp_kses_post($sub_title); ?> </a></h4>
                                <?php echo wp_kses_post($text); ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- About Company END -->
</div>
<!-- contact area  END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>