<?php
ob_start() ;?>

<!-- Our Info -->
<div class="section-full bg-img-fix content-inner-1 overlay-primary-dark" style="background-image: url('<?php echo esc_url(wp_get_attachment_url($bg_img)); ?>');">
    <div class="container">	
        <div class="section-head max-w700 m-auto text-white text-center">
            <h2 class="title-top"><?php echo wp_kses_post($title);?></h2>
            <h2 class="font-50 text-secondry"><?php echo wp_kses_post($text);?></h2>
        </div>
        <div class="row">
            
            <?php $contact_info = (array)json_decode(urldecode($funfact));
				if( $contact_info && is_array($contact_info) ): 
				foreach( (array)$contact_info as $key => $value ):
			?>
            
            <div class="col-md-3 col-sm-6">
                <div class="bg-white our-info p-a25 fly-box-ho">
                    <div class="icon-md pull-left text-secondry"><i class="ti-hand-drag"></i></div>
                    <div class="clearfix text-secondry m-l60">
                        <h3 class="title-name m-a0"><?php echo wp_kses_post(bheem_set( $value, 'title' )); ?></h3>
                        <span><?php echo wp_kses_post(bheem_set( $value, 'info' )); ?></span>
                    </div>
                </div>
            </div>
            
            <?php endforeach; endif;?>
            
        </div>
    </div>
</div>

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>