<?php 
bheem_bunch_global_variable();
$paged = get_query_var('paged');
$args = array('post_type' => 'bunch_projects', 'showposts'=>$num, 'orderby'=>$sort, 'order'=>$order, 'paged'=>$paged);
$terms_array = explode(",",$exclude_cats);
if($exclude_cats) $args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => $terms_array,'operator' => 'NOT IN',));
$query = new WP_Query($args);

$t = $GLOBALS['_bunch_base'];

$data_filtration = '';
$data_posts = '';
?>


<?php if( $query->have_posts() ):
	
ob_start();?>

	<?php $count = 0; 
	$fliteration = array();?>
	<?php while( $query->have_posts() ): $query->the_post();
		global  $post;
		$meta = get_post_meta( get_the_id(), '_bunch_portfolio_meta', true );//printr($meta);
		$meta1 = _WSH()->get_meta();
		$post_terms = get_the_terms( get_the_id(), 'projects_category');// printr($post_terms); exit();
		foreach( (array)$post_terms as $pos_term ) $fliteration[$pos_term->term_id] = $pos_term;
		$temp_category = get_the_term_list(get_the_id(), 'projects_category', '', ', ');
	?>
		<?php $post_terms = wp_get_post_terms( get_the_id(), 'projects_category'); 
		$term_slug = '';
		if( $post_terms ) foreach( $post_terms as $p_term ) $term_slug .= $p_term->slug.'';?>		
           
		   <?php 
			$post_thumbnail_id = get_post_thumbnail_id($post->ID);
			$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
		   ?>
            <li data-filter="<?php echo esc_attr($term_slug); ?>" class="card-container col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <div class="dez-box dez-gallery-box">
                    <div class="dez-thum dez-img-overlay1 dez-img-effect zoom-slow"> <a href="<?php echo esc_url(bheem_set($meta1, 'ext_url')); ?>"> <?php the_post_thumbnail('bheem_480x430'); ?> </a>
                        <div class="overlay-bx">
                            <div class="overlay-icon"> 
                                <h2 class="m-t0 text-white"><?php the_title(); ?></h2>
                                <div class="clearfix m-b15"><div class="dez-separator bg-white"></div></div>
                                <a href="<?php echo esc_url(bheem_set($meta1, 'ext_url')); ?>"> 
                                    <i class="fa fa-link icon-bx-xs radius-sm"></i> 
                                </a> 
                                <a href="<?php echo esc_url($post_thumbnail_url);?>" class="mfp-link" title="<?php the_title_attribute(); ?>"> 
                                    <i class="fa fa-picture-o icon-bx-xs radius-sm"></i> 
                                </a> 
                            </div>
                        </div>
                    </div>
                </div>
            </li>
           
<?php endwhile;?>
  
<?php wp_reset_postdata();
$data_posts = ob_get_contents();
ob_end_clean();

endif; 

ob_start();?>	 

<?php $terms = get_terms(array('projects_category')); ?>

<div class="section-full content-inner">
    <!-- Product details -->
    <div class="container-fluid">
        <!-- Gallery -->
        <div class="site-filters clearfix center  m-b40">
            <ul class="filters" data-toggle="buttons">
                <li data-filter="" class="btn active">
                    <input type="radio">
                    <a href="#" class="site-button-secondry button-skew active"><span><?php esc_html_e('Show All', 'bheem');?></span></a> 
                </li>
                <?php foreach( $fliteration as $t ): ?>
                <li data-filter="<?php echo esc_attr(bheem_set( $t, 'slug' )); ?>" class="btn">
                    <input type="radio" >
                    <a href="#" class="site-button-secondry button-skew"><span><?php echo balanceTags(bheem_set( $t, 'name')); ?></span></a> 
                </li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="row">
            <ul id="masonry" class="dez-gallery-listing gallery-grid-4 mfp-gallery">
                <?php echo $data_posts; ?>
            </ul>
        </div>
        <!-- Gallery END -->
    </div>
    
    <!-- Product details -->
</div>

<?php $output = ob_get_contents();
ob_end_clean(); 
return $output;?>