<?php
ob_start() ;?>

<div class="section-full p-tb50 bg-primary">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="font-35 text-black m-a0"><?php echo wp_kses_post($title); ?></h2>
                <h4 class="font-weight-300 max-w600 m-auto m-tb20"><?php echo wp_kses_post($text); ?></h4>
                <div><a href="<?php echo esc_url($btn_link); ?>" class="site-button radius-no text-secondry button-md outline black"><?php echo wp_kses_post($btn_title); ?></a></div>
            </div>
        </div>
    </div>
</div>

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>