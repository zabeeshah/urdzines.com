<?php
ob_start() ;?>

<!-- About Us -->
<div class="section-full about-us bg-white content-inner">
    <div class="container">
        <div class="row">
            <div class="col-md-5 hidden-sm">
                <img src="<?php echo esc_url(wp_get_attachment_url($image)); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"/>
            </div>
            <div class="col-md-7">
                <div class="m-b20 p-t30">
                    <h2 class="text-uppercase m-t0 m-b10"><?php echo wp_kses_post($contents); ?></h2>
                    <span class="text-gray-dark font-16"><?php echo wp_kses_post($sub_title); ?></span>
                    <div class="clear"></div>
                </div>
                <p class="m-b30"><?php echo wp_kses_post($text); ?></p>
                <div class="row">
                    <?php $skills_array = (array)json_decode(urldecode($funfact));
						if( $skills_array && is_array($skills_array) ): 
						foreach( (array)$skills_array as $key => $value ):
					?>
                    <div class="col-md-4 col-sm-4 m-b15">
                        <div class="icon-bx-wraper bx-style-1 p-tb15 p-lr10 center">
                            <div class="icon-bx-sm radius bg-primary m-b5"> <a href="<?php echo esc_url(bheem_set( $value, 'link' )); ?>" class="icon-cell"><i class="fa <?php echo esc_attr(bheem_set( $value, 'icons' )); ?>"></i></a> </div>
                            <div class="icon-content">
                                <h2 class="text-primary m-t20 m-b15"><span class="counter"><?php echo wp_kses_post(bheem_set( $value, 'counter_value' )); ?></span><?php echo wp_kses_post(bheem_set( $value, 'plus_sign' )); ?></h2>
                                <p><?php echo wp_kses_post(bheem_set( $value, 'title1' )); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; endif;?>
                </div>
                <p class="m-b15"><?php echo wp_kses_post($text1); ?></p>
                <a href="<?php echo esc_url($btn_link); ?>" class="site-button"><?php echo wp_kses_post($btn_title); ?></a>
            </div>
        </div>
    </div>
</div>
<!-- About Us END -->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>