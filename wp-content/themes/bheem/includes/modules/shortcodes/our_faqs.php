<?php
ob_start() ;?>

<!-- Content -->
<div class="page-content">

	<!-- contact area -->
    <div class="content bg-white">
        <!-- Faq -->
         <section class="section-full content-inner-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7">
                        <div class="row">
                            <?php $skills_array = (array)json_decode(urldecode($faq_tab)); 
								if( $skills_array && is_array($skills_array) ):
								$counts = 1; 
								foreach( (array)$skills_array as $key => $value ):
								
								$num = bheem_set($value, 'num');
								$cat = bheem_set($value, 'cat');
								$sort = bheem_set($value, 'sort');
								$order = bheem_set($value, 'order');
								$text_limit = bheem_set($value, 'text_limit');
							?>
                            <div class="col-md-12 m-t30">
                                <h2 class="m-b10 m-t0"><?php echo wp_kses_post(bheem_set( $value, 'title' )); ?></h2>
                                <p><?php echo wp_kses_post(bheem_set( $value, 'text' )); ?></p>
                                
                                <div class="dez-accordion space" id="accordion<?php echo esc_attr($key);?><?php echo esc_attr($counts);?>">
                                    <?php  
									   $count = 1;
									   $query_args = array('post_type' => 'bunch_faqs' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
									   if( $cat ) $query_args['faqs_category'] = $cat;
									   $query = new WP_Query($query_args); 
										
										if($query->have_posts()):
										 
										while($query->have_posts()): $query->the_post();
										global $post ;
										$faq_meta = _WSH()->get_meta();
									?>
                                    <div class="panel">
                                        <div class="acod-head">
                                            <h6 class="acod-title"> 
                                                <a data-toggle="collapse" href="#collapseOne<?php the_id();?>" class="<?php if($count != 1) echo 'collapsed'; ?>" data-parent="#accordion<?php the_id();?>">
                                                    <i class="fa fa-question-circle"></i> <?php the_title(); ?>
                                                </a> 
                                            </h6>
                                        </div>
                                        <div id="collapseOne<?php the_id();?>" class="acod-body collapse <?php if($count == 1) echo 'in'; ?>">
                                            <div class="acod-content"><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></div>
                                        </div>
                                    </div>
                                    <?php $count++; endwhile; endif;?>
                                </div>
                                
                            </div>
                            
                            <?php $count++; endforeach; endif;?>
                        </div>
                        
                        <!-- Faq END -->
                    </div>
                    <div class="col-md-4 col-sm-5 contact-style-1 faqs-form">
                        
                        <div class="p-a30 bg-gray clearfix m-b30">
                            <h2><?php echo wp_kses_post($form_title); ?></h2>
                            <div class="dzFormMsg"></div>
                            <?php echo do_shortcode(bunch_base_decode($contact_form));?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- Faq END -->
    </div>
    <!-- contact area  END -->
</div>
<!-- Content END-->

<?php
	$output = ob_get_contents(); 
    ob_end_clean(); 
    return $output ; ?>