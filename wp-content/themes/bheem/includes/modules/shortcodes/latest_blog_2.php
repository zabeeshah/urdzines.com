<?php  
   global $post ;
   $count = 0;
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>  
<?php if($query->have_posts()):  ?>   

<!-- Latest blog -->
<div class="section-full content-inner-1 ">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="text-uppercase"><?php echo wp_kses_post($title); ?></h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
        </div>
        <div class="section-content">
            <div class="blog-carousel mfp-gallery gallery owl-btn-center-lr">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$post_meta = _WSH()->get_meta();
				?>
                <div class="item">
                    <div class="ow-blog-post date-style-2">
                        <div class="ow-post-media dez-img-effect zoom-slow"> <?php the_post_thumbnail('bheem_700x438'); ?> </div>
                        <div class="ow-post-info ">
                            <div class="ow-post-title">
                                <h4 class="post-title"> <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>" title="<?php esc_attr_e('Video post', 'bheem'); ?>"><?php the_title(); ?></a> </h4>
                            </div>
                            <div class="ow-post-meta">
                                <ul>
                                    <li class="post-date"> <i class="fa fa-calendar"></i><strong><?php echo get_the_date(); ?></strong></li>
                                    <li class="post-author"><i class="fa fa-user"></i><?php esc_html_e('By', 'bheem'); ?> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>" title="<?php esc_attr_e('Posts by demongo', 'bheem' );?>" rel="author"><?php the_author(); ?></a> </li>
                                    <li class="post-comment"><i class="fa fa-comments"></i> <a href="<?php echo esc_url(get_the_permalink(get_the_id()).'#comment');?>" class="comments-link"><?php comments_number( wp_kses_post(__('0 Comments' , 'bheem')), wp_kses_post(__('1 Comment' , 'bheem')), wp_kses_post(__('% Comments' , 'bheem'))); ?></a> </li>
                                </ul>
                            </div>
                            <div class="ow-post-text">
                                <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?></p>
                            </div>
                            <div class="ow-post-readmore "> <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>" title="<?php esc_attr_e('READ MORE', 'bheem'); ?>" rel="bookmark" class=" site-button-link"> <?php esc_html_e('READ MORE', 'bheem'); ?> <i class="fa fa-angle-double-right"></i></a> </div>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
</div>
<!-- Latest blog END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>