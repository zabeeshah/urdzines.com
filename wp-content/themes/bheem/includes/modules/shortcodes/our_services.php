<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<!-- contact area -->
<div class="section-full bg-white content-inner">
    <!-- About Company -->
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <?php if ( is_active_sidebar( $sidebar_slug ) ) : ?>
					<?php dynamic_sidebar( $sidebar_slug ); ?>
                <?php endif; ?>
            </div>
            <div class="col-md-9 col-sm-8">
                <div class="row">
                    <?php while($query->have_posts()): $query->the_post();
						global $post ; 
						$services_meta = _WSH()->get_meta();
					?>
                	<div class="col-md-4 col-sm-6 m-b30">
                        <div class="dez-box">
                            <div class="dez-media"> <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_post_thumbnail('bheem_700x438'); ?></a> </div>
                            <div class="dez-info">
                                <h4 class="dez-title m-t20"><a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h4>
                                <p><?php echo wp_kses_post(bheem_trim(get_the_content(), $text_limit)); ?> </p>
                                <a href="<?php echo esc_url(bheem_set($services_meta, 'ext_url')); ?>" class="site-button"><?php esc_html_e('More', 'bheem'); ?></a> 
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- About Company END -->
</div>
<!-- contact area  END -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>