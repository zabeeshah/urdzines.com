<?php
///---Footer widgets---
//About Us
class Bunch_About_us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_About_us', /* Name */esc_html__('Bheem About Us','bheem'), array( 'description' => esc_html__('Show the information about company', 'bheem' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		
		echo wp_kses_post($before_widget);?>
      		
			<!--Footer Column-->
            <div class="widget_about">
                <div class="logo-footer"><a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo esc_url($instance['footer_logo_img']); ?>" alt="<?php esc_attr_e('image', 'bheem');?>"></a></div>
                <p><?php echo wp_kses_post($instance['content']); ?></p>
                
				<?php if( $instance['show'] ): ?>
                    <ul class="dez-social-icon border">
                        <?php echo wp_kses_post(bheem_get_social_icons()); ?>
                    </ul>
                <?php endif; ?>
            </div>
            
		<?php
		
		echo wp_kses_post($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['footer_logo_img'] = strip_tags($new_instance['footer_logo_img']);
		$instance['content'] = $new_instance['content'];
		$instance['show'] = $new_instance['show'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$footer_logo_img = ($instance) ? esc_attr($instance['footer_logo_img']) : 'http://makaanlelo.com/wp/bheem/wp-content/uploads/2018/02/logo-dark.png';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$show = ( $instance ) ? esc_attr($instance['show']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('footer_logo_img')); ?>"><?php esc_html_e('Footer Logo url:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Logo url', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('footer_logo_img')); ?>" name="<?php echo esc_attr($this->get_field_name('footer_logo_img')); ?>" type="text" value="<?php echo esc_attr($footer_logo_img); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Content:', 'bheem'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo wp_kses_post($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('show')); ?>"><?php esc_html_e('Show Social Icons:', 'bheem'); ?></label>
			<?php $selected = ( $show ) ? ' checked="checked"' : ''; ?>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('show')); ?>"<?php echo esc_attr($selected); ?> name="<?php echo esc_attr($this->get_field_name('show')); ?>" type="checkbox" value="true" />
        </p>        
                
		<?php 
	}
	
}

//Resent Post 
class Bunch_Recent_Post extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Recent_Post', /* Name */esc_html__('Bheem Recent Posts','bheem'), array( 'description' => esc_html__('Show the Recent Posts', 'bheem' )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget); ?>
		
        <!-- Recent Posts -->
        <div class="recent-posts-entry">
            <?php echo wp_kses_post($before_title.$title.$after_title); ?>
            <div class="dez-separator-outer m-b10">
                <div class="dez-separator bg-white style-skew"></div>
            </div>
            <div class="widget-post-bx">
                <?php $query_string = 'posts_per_page='.$instance['number'];
					if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
					 
					$this->posts($query_string);
				?>
            </div>
        </div>
        
		<?php echo wp_kses_post($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : 'Recent Post';
		$number = ( $instance ) ? esc_attr($instance['number']) : 3;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'bheem'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'bheem'), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts($query_string)
	{
		$query = new WP_Query($query_string);
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
			<?php while( $query->have_posts() ): $query->the_post(); ?>
                <div class="widget-post clearfix">
                    <div class="dez-post-media"> <?php the_post_thumbnail('bheem_200x160'); ?> </div>
                    <div class="dez-post-info">
                        <div class="dez-post-header">
                            <h6 class="post-title text-uppercase"><a href="<?php echo esc_url(get_the_permalink(get_the_id()));?>"><?php the_title(); ?></a></h6>
                        </div>
                        <div class="dez-post-meta">
                            <ul>
                                <li class="post-author"><?php esc_html_e('By', 'bheem')?> <?php the_author();?></li>
                                <li class="post-comment"><i class="fa fa-comments"></i> <?php comments_number( '0', '1', '%' ); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            
        <?php endif;
		wp_reset_postdata();
    }
}

//Our Services
class Bunch_services extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_services', /* Name */esc_html__('Bheem Our Services','bheem'), array( 'description' => esc_html__('Show the Our Services', 'bheem' )) );
	}
 
	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget); ?>
		
        <!-- Our Services -->
        <div class="widget_services">
            <?php echo wp_kses_post($before_title.$title.$after_title); ?>
            <div class="dez-separator-outer m-b10">
                <div class="dez-separator bg-white style-skew"></div>
            </div>
            <ul>
                <?php 
					$args = array('post_type' => 'bunch_services', 'showposts'=>$instance['number']);
					if( $instance['cat'] ) $args['tax_query'] = array(array('taxonomy' => 'services_category','field' => 'id','terms' => (array)$instance['cat']));
					 
					$this->posts($args);
					
				?>
            </ul>
        </div>

        <?php echo wp_kses_post($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}
	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : esc_html__('Our Services', 'bheem');
		$number = ( $instance ) ? esc_attr($instance['number']) : 6;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
		
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number of posts: ', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'bheem'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'bheem'), 'selected'=>$cat, 'taxonomy' => 'services_category', 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts($args)
	{
		$query = new WP_Query($args); 
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
            <?php while( $query->have_posts() ): $query->the_post(); 
				global $post ; 
				$service_meta = _WSH()->get_meta();
			?>
            	<li><a href="<?php echo esc_url(bheem_set($service_meta, 'ext_url')); ?>"><?php the_title(); ?></a></li>
            <?php endwhile; ?>
                
        <?php endif;
		wp_reset_postdata();
    }
}

//Contact Us
class Bunch_Contact_us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Contact_us', /* Name */esc_html__('Bheem Footer Contact Us','bheem'), array( 'description' => esc_html__('Show the information about company', 'bheem' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget);?>
      		
			<!--Footer Column-->
            <div class="widget_getintuch">
                <?php echo wp_kses_post($before_title.$title.$after_title); ?>
                <div class="dez-separator-outer m-b10">
                    <div class="dez-separator bg-white style-skew"></div>
                </div>
                <ul>
                    <li><i class="fa fa-map-marker"></i><strong><?php esc_html_e('address', 'bheem'); ?></strong> <?php echo wp_kses_post($instance['address']); ?></li>
                    <li><i class="fa fa-phone"></i><strong><?php esc_html_e('phone', 'bheem'); ?></strong><?php echo wp_kses_post($instance['phone']); ?></li>
                    <!--<li><i class="fa fa-fax"></i><strong><?php esc_html_e('FAX', 'bheem'); ?></strong><?php echo wp_kses_post($instance['fax']); ?></li>-->
                    <li><i class="fa fa-envelope"></i><strong><?php esc_html_e('email', 'bheem'); ?></strong><?php echo wp_kses_post($instance['email']); ?></li>
                </ul>
            </div>
            
		<?php
		
		echo wp_kses_post($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['fax'] = $new_instance['fax'];
		$instance['email'] = $new_instance['email'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : esc_html__('Contact Us', 'bheem');
		$address = ($instance) ? esc_attr($instance['address']) : '';
		$phone = ( $instance ) ? esc_attr($instance['phone']) : '';
		$fax = ( $instance ) ? esc_attr($instance['fax']) : '';
		$email = ( $instance ) ? esc_attr($instance['email']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Contact us', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php esc_html_e('Address:', 'bheem'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('address')); ?>" name="<?php echo esc_attr($this->get_field_name('address')); ?>" ><?php echo wp_kses_post($address); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php esc_html_e('Phone Number:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Phone Number', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('fax')); ?>"><?php esc_html_e('Fax Number:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Fax', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('fax')); ?>" name="<?php echo esc_attr($this->get_field_name('fax')); ?>" type="text" value="<?php echo esc_attr($fax); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email Address:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Email', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>        
                
		<?php 
	}
	
}


///---Dynamic Sidebar widgets---
//Services Sidebar
class Bunch_services_sidebar extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_services_sidebar', /* Name */esc_html__('Bheem Services Sidebar','bheem'), array( 'description' => esc_html__('Show the Services Sidebar Widget', 'bheem' )) );
	}
 
	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		
		echo wp_kses_post($before_widget); ?>
		
        <!-- Our Services -->
        <div class="widget_services style-2 m-b40">
            <ul>
                <?php 
					$args = array('post_type' => 'bunch_services', 'showposts'=>$instance['number']);
					if( $instance['cat'] ) $args['tax_query'] = array(array('taxonomy' => 'services_category','field' => 'id','terms' => (array)$instance['cat']));
					 
					$this->posts($args);
					
				?>
            </ul>
        </div>

        <?php echo wp_kses_post($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}
	/** @see WP_Widget::form */
	function form($instance)
	{
		$number = ( $instance ) ? esc_attr($instance['number']) : 6;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
		
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number of posts: ', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'bheem'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'bheem'), 'selected'=>$cat, 'taxonomy' => 'services_category', 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts($args)
	{
		
		$query = new WP_Query($args);
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
            <?php while( $query->have_posts() ): $query->the_post(); 
				global $post ; 
				$service_meta = _WSH()->get_meta();
			?>
            <li><a href="<?php echo esc_url(bheem_set($service_meta, 'ext_url')); ?>"><?php the_title(); ?></a> </li>
            <?php endwhile; ?>
                
        <?php endif;
		wp_reset_postdata();
    }
}

/// Our Brochures
class Bunch_Brochures extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Brochures', /* Name */esc_html__('Consruct Zilla Our Brochures','bheem'), array( 'description' => esc_html__('Show the info Our Brochures', 'bheem' )) );
	}
	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget);?>
      		
            <!--Our Brochures-->
            <div class="widget">
                <?php echo wp_kses_post($before_title.$title.$after_title); ?>
                <div class="download-file">
                    <ul>
                        <li>
                            <a href="<?php echo esc_url($instance['pdf']); ?>" target="_blank">
                                <span><i class="fa fa-file-pdf-o"></i></span>
                                <text><?php esc_html_e('Company Brochures', 'bheem'); ?></text>
                                <i class="fa fa-download"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url($instance['pdf_two']); ?>" target="_blank">
                                <span><i class="fa fa-file-pdf-o"></i></span>
                                <text><?php esc_html_e('Company Info', 'bheem'); ?></text>
                                <i class="fa fa-download"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            
		<?php
		
		echo wp_kses_post($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['pdf'] = $new_instance['pdf'];
		$instance['pdf_two'] = $new_instance['pdf_two'];
		return $instance;
	}
	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : esc_html__('Get your brochures', 'bheem');
		$pdf = ( $instance ) ? esc_attr($instance['pdf']) : '#';
		$pdf_two = ($instance) ? esc_attr($instance['pdf_two']) : '#';
		?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('pdf')); ?>"><?php esc_html_e('Company Brochures PDF Link:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('PDF link here', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('pdf')); ?>" name="<?php echo esc_attr($this->get_field_name('pdf')); ?>" type="text" value="<?php echo esc_attr($pdf); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('pdf_two')); ?>"><?php esc_html_e('Company Info PDF Link:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('PDF link here', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('pdf_two')); ?>" name="<?php echo esc_attr($this->get_field_name('pdf_two')); ?>" type="text" value="<?php echo esc_attr($pdf_two); ?>" />
        </p>
                
		<?php 
	}
	
}

//Sidebar Contact Us
class Bunch_Sidebar_Contact_us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Sidebar_Contact_us', /* Name */esc_html__('Bheem Sidebar Contact Us','bheem'), array( 'description' => esc_html__('Show the information about company', 'bheem' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget);?>
      		
			<!--Footer Column-->
            <div class="widget widget_getintuch">
				<?php echo wp_kses_post($before_title.$title.$after_title); ?>
                <ul>
                    <li><i class="fa fa-map-marker"></i><strong><?php esc_html_e('address', 'bheem'); ?></strong> <?php echo wp_kses_post($instance['address']); ?></li>
                    <li><i class="fa fa-phone"></i><strong><?php esc_html_e('phone', 'bheem'); ?></strong><?php echo wp_kses_post($instance['phone']); ?></li>
                    <li><i class="fa fa-envelope"></i><strong><?php esc_html_e('email', 'bheem'); ?></strong><?php echo wp_kses_post($instance['email']); ?></li>
                </ul>
            </div>
            
		<?php
		
		echo wp_kses_post($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['email'] = $new_instance['email'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : 'Contact us';
		$address = ($instance) ? esc_attr($instance['address']) : '';
		$phone = ( $instance ) ? esc_attr($instance['phone']) : '';
		$email = ( $instance ) ? esc_attr($instance['email']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Contact us', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php esc_html_e('Address:', 'bheem'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('address')); ?>" name="<?php echo esc_attr($this->get_field_name('address')); ?>" ><?php echo wp_kses_post($address); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php esc_html_e('Phone and Fax Number:', 'bheem'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" ><?php echo wp_kses_post($phone); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email Address:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Email', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>        
                
		<?php 
	}
	
}


///---Projects Sidebar widgets---
//Recent Highlights
class Bunch_Recent_Highlights extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Recent_Highlights', /* Name */esc_html__('Bheem Recent Highlights','bheem'), array( 'description' => esc_html__('Show the Recent Highlights', 'bheem' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		
		echo wp_kses_post($before_widget);?>
      		
			<!--Footer Column-->
            <div class="icon-bx-wraper bx-style-1 p-a30 center m-b15">
                <?php if($instance['link']): ?>
                    <div class="icon-bx-sm text-primary bg-white radius border-2 m-b20"> 
                    	<a href="<?php echo esc_url($instance['link']); ?>" class="icon-cell"><i class="fa fa-user"></i></a> 
                    </div>
				<?php endif; ?>
                <div class="icon-content">
                    <h5 class="dez-tilte text-uppercase"><?php echo wp_kses_post($instance['title']); ?></h5>
                    <p><?php echo wp_kses_post($instance['content']); ?></p>
                </div>
            </div>
            
		<?php
		
		echo wp_kses_post($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['content'] = $new_instance['content'];
		$instance['link'] = $new_instance['link'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : esc_html__('Recent Highlights', 'bheem');
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$link = ( $instance ) ? esc_attr($instance['link']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('Recent Highlights', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Contents:', 'bheem'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo wp_kses_post($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('link')); ?>"><?php esc_html_e('External Link:', 'bheem'); ?></label>
            <input placeholder="<?php esc_attr_e('#', 'bheem');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('link')); ?>" name="<?php echo esc_attr($this->get_field_name('link')); ?>" type="text" value="<?php echo esc_attr($link); ?>" />
        </p>       
                
		<?php 
	}
	
}

///---Blog Sidebar widgets---
//Blog Resent Post 
class Bunch_Blog_Recent_Post extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Blog_Recent_Post', /* Name */esc_html__('Bheem Blog Recent Posts','bheem'), array( 'description' => esc_html__('Show the Blog Recent Posts', 'bheem' )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget); ?>
		
        <!-- Recent Posts -->
        <div class="recent-posts-entry">
            <?php echo wp_kses_post($before_title.$title.$after_title); ?>
            <div class="widget-post-bx">
                <?php $query_string = 'posts_per_page='.$instance['number'];
					if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
					 
					$this->posts($query_string);
				?>
            </div>
        </div>
        
		<?php echo wp_kses_post($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : 'Recent Post';
		$number = ( $instance ) ? esc_attr($instance['number']) : 3;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'bheem'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'bheem'), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts($query_string)
	{
		$query = new WP_Query($query_string); 
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
			<?php while( $query->have_posts() ): $query->the_post(); ?>
                <div class="widget-post clearfix">
                    <div class="dez-post-media"> <?php the_post_thumbnail('bheem_200x160'); ?> </div>
                    <div class="dez-post-info">
                        <div class="dez-post-header">
                            <h6 class="post-title"><?php the_title(); ?></h6>
                        </div>
                        <div class="dez-post-meta">
                            <ul>
                                <li class="post-author"><?php esc_html_e('By', 'bheem')?> <?php the_author();?></li>
                                <li class="post-comment"><i class="fa fa-comments"></i> <?php comments_number( '0', '1', '%' ); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            
        <?php endif;
		wp_reset_postdata();
    }
}

//Our Projects
class Bunch_projects extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_projects', /* Name */esc_html__('Bheem Our Projects','bheem'), array( 'description' => esc_html__('Show the Our Projects', 'bheem' )) );
	}
 
	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget); ?>
		
        <!-- Our Projects -->
        <div class="widget_gallery">
            <?php echo wp_kses_post($before_title.$title.$after_title); ?>
            <ul>
                <?php 
					$args = array('post_type' => 'bunch_projects', 'showposts'=>$instance['number']);
					if( $instance['cat'] ) $args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => (array)$instance['cat']));
					 
					$this->posts($args);
				?>
            </ul>
        </div>

        <?php echo wp_kses_post($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}
	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : esc_html__('Our Projects', 'bheem');
		$number = ( $instance ) ? esc_attr($instance['number']) : 6;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
		
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number of posts: ', 'bheem'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'bheem'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'bheem'), 'selected'=>$cat, 'taxonomy' => 'projects_category', 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts($args)
	{
		
		$query = new WP_Query($args); 
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
            <?php while( $query->have_posts() ): $query->the_post(); 
				global $post ; 
				$gallery_meta = _WSH()->get_meta();
			?>
            <li class="img-effect2"> <a href="<?php echo esc_url(bheem_set($gallery_meta, 'ext_url')); ?>"><?php the_post_thumbnail('bheem_85x85'); ?></a> </li>	
            <?php endwhile; ?>
                
        <?php endif;
		wp_reset_postdata();
    }
}