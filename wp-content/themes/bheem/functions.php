<?php
add_action('after_setup_theme', 'bheem_bunch_theme_setup');
function bheem_bunch_theme_setup()
{
	global $wp_version;
	if(!defined('BHEEM_VERSION')) define('BHEEM_VERSION', '1.0');
	if( !defined( 'BHEEM_ROOT' ) ) define('BHEEM_ROOT', get_template_directory().'/');
	if( !defined( 'BHEEM_URL' ) ) define('BHEEM_URL', get_template_directory_uri().'/');	
	include_once get_template_directory() . '/includes/loader.php';
	
	
	load_theme_textdomain('bheem', get_template_directory() . '/languages');
	
	//ADD THUMBNAIL SUPPORT
	add_theme_support('post-thumbnails');
	add_theme_support('woocommerce');
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('widgets'); //Add widgets and sidebar support
	add_theme_support( "title-tag" );
	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'align-wide' );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	/** Register wp_nav_menus */
	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'main_menu' => esc_html__('Main Menu', 'bheem'),
				'footer_menu' => esc_html__('Footer Menu', 'bheem'),
			)
		);
	}
	if ( ! isset( $content_width ) ) $content_width = 960;
	add_image_size( 'bheem_700x438', 700, 438, true ); // 'bheem_700x438 Latest Blog'
	add_image_size( 'bheem_100x100', 100, 100, true ); // 'bheem_100x100 Our Testimonials'
	add_image_size( 'bheem_500x730', 500, 730, true ); // 'bheem_500x730 Meet Our Team 2'
	add_image_size( 'bheem_723x389', 723, 389, true ); // 'bheem_723x389 Our Best Services'
	add_image_size( 'bheem_700x466', 700, 466, true ); // 'bheem_700x466 Latest Projects'
	add_image_size( 'bheem_700x438', 700, 438, true ); // 'bheem_700x438 Our Services'
	add_image_size( 'bheem_200x160', 200, 160, true ); // 'bheem_200x160 Recent Post'
	add_image_size( 'bheem_480x430', 480, 430, true ); // 'bheem_480x430 Our Projects 3'
	add_image_size( 'bheem_700x500', 700, 500, true ); // 'bheem_700x500 Blog List View'
	add_image_size( 'bheem_500x500', 500, 500, true ); // 'bheem_500x500 Blog List View'
	add_image_size( 'bheem_85x85', 85, 85, true ); // 'bheem_85x85 Our Projects'
}

function bheem_gutenberg_editor_palette_styles() {
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'strong yellow', 'bheem' ),
            'slug' => 'strong-yellow',
            'color' => '#fdc716',
        ),
        array(
            'name' => __( 'strong white', 'bheem' ),
            'slug' => 'strong-white',
            'color' => '#fff',
        ),
		array(
            'name' => __( 'light black', 'bheem' ),
            'slug' => 'light-black',
            'color' => '#333333',
        ),
        array(
            'name' => __( 'very light gray', 'bheem' ),
            'slug' => 'very-light-gray',
            'color' => '#767676',
        ),
        array(
            'name' => __( 'very dark black', 'bheem' ),
            'slug' => 'very-dark-black',
            'color' => '#000000',
        ),
    ) );
	
	add_theme_support( 'editor-font-sizes', array(
		array(
			'name' => __( 'Small', 'bheem' ),
			'size' => 10,
			'slug' => 'small'
		),
		array(
			'name' => __( 'Normal', 'bheem' ),
			'size' => 15,
			'slug' => 'normal'
		),
		array(
			'name' => __( 'Large', 'bheem' ),
			'size' => 24,
			'slug' => 'large'
		),
		array(
			'name' => __( 'Huge', 'bheem' ),
			'size' => 36,
			'slug' => 'huge'
		)
	) );
	
}
add_action( 'after_setup_theme', 'bheem_gutenberg_editor_palette_styles' );

function bheem_bunch_widget_init()
{
	global $wp_registered_sidebars;
	$theme_options = _WSH()->option();
	
	register_sidebar(array(
	  'name' => esc_html__( 'Default Sidebar', 'bheem' ),
	  'id' => 'default-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown on the right-hand side.', 'bheem' ),
	  'before_widget'=>'<div id="%1$s" class="widget sidebar-widget %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<h4 class="widget-title">',
	  'after_title' => '</h4>'
	));
	register_sidebar(array(
	  'name' => esc_html__( 'Footer Sidebar', 'bheem' ),
	  'id' => 'footer-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown in Footer Area.', 'bheem' ),
	  'before_widget'=>'<div class="col-md-3 col-sm-6 footer-col-4"><div id="%1$s" class="widget footer-widget %2$s">',
	  'after_widget'=>'</div></div>',
	  'before_title' => '<h4 class="m-b15 text-uppercase">',
	  'after_title' => '</h4>'
	));
	
	register_sidebar(array(
	  'name' => esc_html__( 'Blog Listing', 'bheem' ),
	  'id' => 'blog-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown on the right-hand side.', 'bheem' ),
	  'before_widget'=>'<div id="%1$s" class="widget sidebar-widget %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<h4 class="widget-title">',
	  'after_title' => '</h4>'
	));
	
	register_sidebar(array(
	  'name' => esc_html__( 'Bheem Header Cart', 'bheem' ),
	  'id' => 'header-cart',
	  'description' => esc_html__( 'Widgets in this area will be shown in Header Area.', 'bheem' ),
	  'before_widget'=>'<div id="%1$s" class="%2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<h3>',
	  'after_title' => '</h3>'
	));
	
	if( !is_object( _WSH() )  )  return;
	$sidebars = bheem_set(bheem_set( $theme_options, 'dynamic_sidebar' ) , 'dynamic_sidebar' ); 
	foreach( array_filter((array)$sidebars) as $sidebar)
	{
		if(bheem_set($sidebar , 'topcopy')) continue ;
		
		$name = bheem_set( $sidebar, 'sidebar_name' );
		
		if( ! $name ) continue;
		$slug = bheem_bunch_slug( $name ) ;
		
		register_sidebar( array(
			'name' => $name,
			'id' =>  sanitize_title( $slug ) ,
			'before_widget' => '<div id="%1$s" class="widget sidebar-widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h4 class="widget-title">',
			'after_title' => '</h4>',
		) );		
	}
	
	update_option('wp_registered_sidebars' , $wp_registered_sidebars) ;
}
add_action( 'widgets_init', 'bheem_bunch_widget_init' );
// Update items in cart via AJAX
function bheem_load_head_scripts() {
	$options = _WSH()->option();
    if ( !is_admin() ) {
	$protocol = is_ssl() ? 'https://' : 'http://';
	$map_path = '?key='.bheem_set($options, 'map_api_key');	
		wp_enqueue_script( 'bheem-map-api', ''.$protocol.'maps.google.com/maps/api/js'.$map_path, array(), false, false );
	}
}
    add_action( 'wp_enqueue_scripts', 'bheem_load_head_scripts' );
//global variables
function bheem_bunch_global_variable() {
    global $wp_query;
}

function bheem_enqueue_scripts() {
	
	$theme_options = _WSH()->option();
	$maincolor = str_replace( '#', '' , bheem_set( $theme_options, 'main_color_scheme', 'fdc716' ) );
	//styles
	if( bheem_set($theme_options, 'rtl') ){
	wp_enqueue_style( 'bootstrap-rtl', get_template_directory_uri() . '/css/bootstrap-rtl.min.css' );
	} else {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	}
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	if( bheem_set($theme_options, 'rtl') ){
	wp_enqueue_style( 'font-awesome-rtl', get_template_directory_uri() . '/css/font-awesome-rtl.css' );
	} 
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css' );
	wp_enqueue_style( 'bootstrap-select', get_template_directory_uri() . '/css/bootstrap-select.min.css' );
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css' );
	wp_enqueue_style( 'bheem-main-style', get_stylesheet_uri() );
	wp_enqueue_style( 'bheem-custom-style', get_template_directory_uri() . '/css/custom.css' );
	wp_enqueue_style( 'bheem-skin-1', get_template_directory_uri() . '/css/skin/skin-1.css' );
	wp_enqueue_style( 'bheem-templete2', get_template_directory_uri() . '/css/templete-2.css' );
	wp_enqueue_style( 'bheem-templete', get_template_directory_uri() . '/css/templete.css' );
	if( bheem_set($theme_options, 'rtl') ){
	wp_enqueue_style( 'bheem-style-rtl', get_template_directory_uri() . '/css/style-rtl.css' );
	}
	if(class_exists('woocommerce')) wp_enqueue_style( 'bheem_woocommerce', get_template_directory_uri() . '/css/woocommerce.css' );
	wp_enqueue_style( 'bheem-gutenberg', get_template_directory_uri() . '/css/gutenberg.css' );
	wp_enqueue_style( 'bheem-responsive', get_template_directory_uri() . '/css/responsive.css' );
	wp_enqueue_style( 'bheem-main-color', get_template_directory_uri() . '/css/color.php?main_color='.$maincolor );
	wp_enqueue_style( 'bheem-color-panel', get_template_directory_uri() . '/css/color-panel.css' );
	
	
    //scripts
	
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js', array(), false, true );
	wp_enqueue_script( 'bootstrap-select', get_template_directory_uri().'/js/bootstrap-select.min.js', array(), false, true );
	wp_enqueue_script( 'bootstrap-touchspin', get_template_directory_uri().'/js/jquery.bootstrap-touchspin.js', array(), false, true );
	wp_enqueue_script( 'magnific-popup', get_template_directory_uri().'/js/magnific-popup.js', array(), false, true );
	wp_enqueue_script( 'waypoints-min', get_template_directory_uri().'/js/waypoints-min.js', array(), false, true );
	wp_enqueue_script( 'counterup', get_template_directory_uri().'/js/counterup.min.js', array(), false, true );
	wp_enqueue_script( 'jquery-countdown', get_template_directory_uri().'/js/jquery.countdown.js', array('jquery'), '2.1.2', true );
	wp_enqueue_script( 'custom-masonry', get_template_directory_uri().'/js/custom-masonry.js', array(), false, true );
	wp_enqueue_script( 'custom-loder', get_template_directory_uri().'/js/custom-imagesloaded.js', array(), false, true );
	wp_enqueue_script( 'masonry-filter', get_template_directory_uri().'/js/masonry.filter.js', array(), false, true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri().'/js/owl.carousel.js', array(), false, true );
	wp_enqueue_script( 'bheem-main-script', get_template_directory_uri().'/js/custom.js', array(), false, true );
	if( bheem_set($theme_options, 'rtl') ){
	wp_enqueue_script( 'dz-carousel-rtl', get_template_directory_uri().'/js/dz.carousel-rtl.js', array(), false, true );
	} else {
	wp_enqueue_script( 'dz-carousel', get_template_directory_uri().'/js/dz.carousel.js', array(), false, true );
	}
	wp_enqueue_script( 'bheem-map-script', get_template_directory_uri().'/js/map-script.js', array(), false, true );
	
	wp_enqueue_script( 'bheem-jquery-cookie', get_template_directory_uri().'/js/jquery.cookie.js', array(), false, true );
	
	if( is_singular() ) wp_enqueue_script('comment-reply');
	
}
add_action( 'wp_enqueue_scripts', 'bheem_enqueue_scripts' );

/*-------------------------------------------------------------*/
function bheem_theme_slug_fonts_url() {
    $fonts_url = '';
 
    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $montserrat = _x( 'on', 'Montserrat font: on or off', 'bheem' );
	$nunito = _x( 'on', 'Nunito font: on or off', 'bheem' );
	$open_sans = _x( 'on', 'Open Sans font: on or off', 'bheem' );
	$oswald = _x( 'on', 'Oswald font: on or off', 'bheem' );
	$pt_serif = _x( 'on', 'PT+Serif font: on or off', 'bheem' );
	$quicksand = _x( 'on', 'Quicksand font: on or off', 'bheem' );
	$roboto_slab = _x( 'on', 'Roboto Slab font: on or off', 'bheem' );
	$roboto = _x( 'on', 'Roboto font: on or off', 'bheem' );
	
    /* Translators: If there are characters in your language that are not
    * supported by Open Sans, translate this to 'off'. Do not translate
    * into your own language.
    */
    $open_sans = _x( 'on', 'Open Sans font: on or off', 'bheem' );
 
    if ( 'off' !== $montserrat || 'off' !== $nunito || 'off' !== $open_sans || 'off' !== $oswald || 'off' !== $pt_serif || 'off' !== $quicksand || 'off' !== $roboto_slab || 'off' !== $roboto ) {
        $font_families = array();
 
        if ( 'off' !== $montserrat ) {
            $font_families[] = 'Montserrat:200,300,400,500,600,700,800,900';
        }
		
		if ( 'off' !== $nunito ) {
            $font_families[] = 'Nunito:200,300,400,600,700,800,900';
        }
		
		if ( 'off' !== $open_sans ) {
            $font_families[] = 'Open Sans:300,400,600,700,800';
        }
		
		if ( 'off' !== $oswald ) {
            $font_families[] = 'Oswald:200,300,400,500,600,700';
        }
		
		if ( 'off' !== $pt_serif ) {
            $font_families[] = 'PT Serif:400,700';
        }
		
		if ( 'off' !== $quicksand ) {
            $font_families[] = 'Quicksand:300,400,500,700';
        }
		
		if ( 'off' !== $roboto_slab ) {
            $font_families[] = 'Roboto Slab:100,300,400';
        }
		
		if ( 'off' !== $roboto ) {
            $font_families[] = 'Roboto:100,300,400,500,700,900';
        }
 		
		$opt = get_option('bheem'.'_theme_options');
		if ( bheem_set( $opt, 'body_custom_font' ) ) {
			if ( $custom_font = bheem_set( $opt, 'body_font_family' ) )
				$font_families[] = $custom_font . ':300,300i,400,400i,600,700';
		}
		if ( bheem_set( $opt, 'use_custom_font' ) ) {
			$font_families[] = bheem_set( $opt, 'h1_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = bheem_set( $opt, 'h2_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = bheem_set( $opt, 'h3_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = bheem_set( $opt, 'h4_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = bheem_set( $opt, 'h5_font_family' ) . ':300,300i,400,400i,600,700';
			$font_families[] = bheem_set( $opt, 'h6_font_family' ) . ':300,300i,400,400i,600,700';
		}
		$font_families = array_unique( $font_families);
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }
 
    return esc_url_raw( $fonts_url );
}
function bheem_theme_slug_scripts_styles() {
    wp_enqueue_style( 'bheem-theme-slug-fonts', bheem_theme_slug_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'bheem_theme_slug_scripts_styles' );
/*---------------------------------------------------------------------*/
function bheem_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'bheem_add_editor_styles' );
/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
function bheem_woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 6;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'bheem_jk_related_products_args' );
function bheem_jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

/*js Template Path */
function bheem_set_js_var() {
   $theme_options = _WSH()->option();
   $header_style = bheem_set( $theme_options, 'header_style' );
   $cart_on_mobile = 0;
   if($header_style == 'header_v1')
   {
	    $cart_on_mobile = bheem_set( $theme_options, 'cart_button_on_mobile_1' );
   }else if($header_style == 'header_v2')
   {
	   $cart_on_mobile = bheem_set( $theme_options, 'cart_button_on_mobile_2' );
   }else if($header_style == 'header_v3')
   {
	   $cart_on_mobile = bheem_set( $theme_options, 'cart_button_on_mobile_3' );
	   
   }else if($header_style == 'header_v5')
   {
	   $cart_on_mobile = bheem_set( $theme_options, 'cart_button_on_mobile_5' );
	}
   
   if($cart_on_mobile)
   {
	   $cart_on_mobile = 1;
   }else{
	   $cart_on_mobile = 0;
   }
  
   $js_var_array = array('cart_on_mobile' => $cart_on_mobile);
   wp_localize_script( 'jquery', 'bheem_data', $js_var_array );
} 
add_action('wp_enqueue_scripts','bheem_set_js_var');