<form action="<?php echo esc_url(home_url('/')); ?>" method="get">
    <input name="s" value="" type="text" class="form-control" placeholder="<?php esc_attr_e('Type to search', 'bheem');?>">
    <span id="quik-search-remove"><i class="fa fa-remove"></i></span>
</form>