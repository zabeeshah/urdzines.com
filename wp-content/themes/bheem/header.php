<?php $options = _WSH()->option();
	bheem_bunch_global_variable();
	$icon_href = (bheem_set( $options, 'site_favicon' )) ? bheem_set( $options, 'site_favicon' ) : get_template_directory_uri().'/images/favicon.png';
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
     <!-- Basic -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- Favcon -->
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ):?>
        <link rel="shortcut icon" type="image/png" href="<?php echo esc_url($icon_href);?>">
    <?php endif;?>
    <?php wp_head(); ?>
</head>

<body <?php if( bheem_set($options, 'boxed') ) body_class('boxed'); else body_class(); ?>>

	<?php
		if(function_exists('wp_body_open'))
		{
			wp_body_open(); 
		}else{
			do_action( 'wp_body_open' );
		}
	?>

    <div class="page-wraper">
		
        <?php if(bheem_set($options, 'preloader')):?>
            <!-- Preloader -->
            <div id="loading-area"></div>
        <?php endif;?>
        
        <?php $header = bheem_set($options, 'header_style');
			$header_style = bheem_set( $options, 'header_style' );
			$header_style1 = bheem_set($_GET, 'header_style');
			$header_style2 = ( $header_style1) ? $header_style1 : $header_style;
			$header_style = ( $header_style2) ? $header_style2 : 'header_v1';
			
			if ( 'header_v2' === $header_style ) :
			
			get_template_part( 'includes/modules/header_v2' );
			
			elseif('header_v3' === $header_style):
			
			get_template_part( 'includes/modules/header_v3' );
			
			elseif('header_v4' === $header_style):
			
			get_template_part( 'includes/modules/header_v4' );
			
			elseif('header_v5' === $header_style):
			
			get_template_part( 'includes/modules/header_v5' );
			
			else:
			
			get_template_part( 'includes/modules/header_v1' );
			endif;
		
		?>