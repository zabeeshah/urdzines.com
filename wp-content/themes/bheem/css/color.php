<?php


/** Set ABSPATH for execution */
define( 'ABSPATH', dirname(dirname(__FILE__)) . '/' );
define( 'WPINC', 'wp-includes' );


/**
 * @ignore
 */
function add_filter() {}

/**
 * @ignore
 */
function esc_attr($str) {return $str;}

/**
 * @ignore
 */
function apply_filters() {}

/**
 * @ignore
 */
function get_option() {}

/**
 * @ignore
 */
function is_lighttpd_before_150() {}

/**
 * @ignore
 */
function add_action() {}

/**
 * @ignore
 */
function did_action() {}

/**
 * @ignore
 */
function do_action_ref_array() {}

/**
 * @ignore
 */
function get_bloginfo() {}

/**
 * @ignore
 */
function is_admin() {return true;}

/**
 * @ignore
 */
function site_url() {}

/**
 * @ignore
 */
function admin_url() {}

/**
 * @ignore
 */
function home_url() {}

/**
 * @ignore
 */
function includes_url() {}

/**
 * @ignore
 */
function wp_guess_url() {}

if ( ! function_exists( 'json_encode' ) ) :
/**
 * @ignore
 */
function json_encode() {}
endif;



/* Convert hexdec color string to rgb(a) string */
 
function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}
$yellow = $_GET['main_color'];

ob_start(); ?>

.bg-primary,
.meet-ask-row:after,
.dez-separator.style-skew[class*="style-"]:after,
.dez-separator.style-skew[class*="style-"]:before,
.dez-tilte-inner.skew-title:after,
.date-style-2 .post-date,
.date-style-3.skew-date .post-date,
.date-style-3.skew-date .post-date:before,
.overlay-primary-light:after,
.overlay-primary-middle:after,
.overlay-primary-dark:after,
.widget-title:after,
.site-button,
.comments-area .comment-form p input[type="submit"],
.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus,
.footer-line:after,
.testimonial-1 .quote-left:before,
.testimonial-2 .quote-left:before,
.site-filters .active > [class*="site-button"],
.list-num-count > li:before,
#myNavbar li.active a, 
.skew-angle:after,
.header-style-6.style-1 .header-nav .nav > li.active a:before,
.header-style-6.style-1 .header-nav .nav > li:hover > a:before,
.dez-project-left:after,
.service-carousel .owl-prev, 
.service-carousel .owl-next,
.about-us .dez-tabs .nav-tabs li.active a,
#myNavbar li.active a,
.sidenav ul li a:hover,
.widget_calendar table tbody td#today,
.searchform input[type="submit"],
.scroltop.style5:after,
.tag-new,
.download-file ul li a > span,
.product-item .overlay-icon a i,
.woocommerce-pagination ul li a:hover, .woocommerce-pagination ul li span, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current,
form.cart button.add-to-cart,
.woocommerce div.product .woocommerce-tabs ul.tabs li a::before,
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #place_order,
.woocommerce #respond input#submit.disabled, .woocommerce #respond input#submit:disabled, .woocommerce #respond input#submit[disabled]:disabled, .woocommerce a.button.disabled, .woocommerce a.button:disabled, .woocommerce a.button[disabled]:disabled, .woocommerce button.button.disabled, .woocommerce button.button:disabled, .woocommerce button.button[disabled]:disabled, .woocommerce input.button.disabled, .woocommerce input.button:disabled, .woocommerce input.button[disabled]:disabled,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
.pagination .page-numbers.current, .pagination a.page-numbers:hover,
.comments-area .comment-form p input[type="submit"], .comments-area .comment-form p button,
.box-number:after
{

	background-color: #<?php echo esc_attr($yellow); ?>;
}

/*Hex color :hover */
.site-button:active,
.site-button:hover,
.site-button:focus,
.active > .site-button,
.bg-primary-dark,
.pagination > li > a:hover, 
.pagination > li > span:hover, 
.pagination > li > a:focus, 
.pagination > li > span:focus ,
.pagination > .active > a, 
.pagination > .active > span, 
.pagination > .active > a:hover, 
.pagination > .active > span:hover, 
.pagination > .active > a:focus, 
.pagination > .active > span:focus,
a.bg-primary:hover,
.woocommerce #review_form #respond .form-submit input:hover,
.thm-unit-test .posts-nav a:hover span  {
    background-color: #<?php echo esc_attr($yellow); ?>;
}

a,
.text-primary,
.primary li:before,
.breadcrumb-row ul li a,
.header-nav .nav > li.active > a,
.header-nav .nav > li.current-menu-item > a .header-nav .nav > li .sub-menu li a:hover,
.header-nav .nav > li:hover > a,
.header-nav .nav > li .mega-menu > li ul a:hover,
.header-nav .nav > li .sub-menu li:hover > a,
.nav-dark.header-nav .nav > li .sub-menu li:hover > a,
.nav-dark.header-nav .nav > li .mega-menu > li ul a:hover,
blockquote:before,
ol.comment-list li.comment .reply a,
footer a:active,
footer a:focus,
footer a:hover,
footer h1 a,
footer h2 a,
footer h3 a,
footer h4 a,
footer h5 a,
footer h6 a,
footer p a,
button.scroltop,
.testimonial-1 .testimonial-position,
.testimonial-4 .testimonial-name:after,
.testimonial-2 .testimonial-position,
.testimonial-3 .testimonial-position,
.testimonial-3 .quote-left:before,
.acod-head a:after,
.acod-head a,
.acod-head a:hover,
.acod-head a.collapsed:hover,
.dez-tabs .nav-tabs > li > a i,
h1 a:hover,
h2 a:hover,
h3 a:hover,
h4 a:hover,
h5 a:hover,
h6 a:hover,
.download-file ul li a > text, .download-file ul li a > i,
.woocommerce div.product .woocommerce-tabs ul.tabs li:hover a, .woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce p.stars a,
.woocommerce .woocommerce-message::before,
.thm-unit-test .posts-nav a:hover,
.woocommerce .woocommerce-info a,.woocommerce .woocommerce-info::before,
.dez-post-readmore a.site-button-link:hover,
.blog-post.blog-lg .dez-post-readmore a.site-button-link:hover,
ol.comment-list li.comment .reply a, .comment-list .comment .reply a
{
	color: #<?php echo esc_attr($yellow); ?>;
}

.navbar-toggle,
.testimonial-4 .testimonial-pic,
.testimonial-4:after,
.testimonial-4 [class*="quote-"],
button.scroltop,
blockquote,.download-file ul li a,
.woocommerce-pagination ul li a:hover, .woocommerce-pagination ul li span, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current,
form.cart button.add-to-cart,
form.cart button.add-to-cart:hover,
.woocommerce #review_form #respond .form-submit input:hover,
.woocommerce .woocommerce-message,
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #place_order,
.woocommerce .woocommerce-error, .woocommerce .woocommerce-info, .woocommerce .woocommerce-message,
.woocommerce #respond input#submit.disabled, .woocommerce #respond input#submit:disabled, .woocommerce #respond input#submit[disabled]:disabled, .woocommerce a.button.disabled, .woocommerce a.button:disabled, .woocommerce a.button[disabled]:disabled, .woocommerce button.button.disabled, .woocommerce button.button:disabled, .woocommerce button.button[disabled]:disabled, .woocommerce input.button.disabled, .woocommerce input.button:disabled, .woocommerce input.button[disabled]:disabled,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
.pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover, .pagination > li > .current, .pagination > li:hover > .current,
.blog-post.blog-lg .dez-post-readmore a.site-button-link:hover
{
	border-color: #<?php echo esc_attr($yellow); ?>;
}

.about-us .dez-tabs .nav-tabs li.active{
	border-right: 4px solid #<?php echo esc_attr($yellow); ?>;
}

.btn-style-two,
.btn-style-one:hover,
.btn-style-three:hover,
.footer-bottom .footer-nav li a:hover,
.sidebar .latest-tweets .owl-prev:hover,
.sidebar .latest-tweets .owl-next:hover
{
	color: #<?php echo esc_attr($yellow); ?> !important;
}

.dez-team-1 .team-info:after,
.dez-team-1.left .team-info:before, 
.dez-team-1.right .team-info:before
{
	border-color: #<?php echo esc_attr($yellow); ?> !important;
}


.dez-info-has.bg-primary
{
	background-color: <?php echo hex2rgba($yellow, 0.9);?> !important;
}
<?php 

$out = ob_get_clean();
$expires_offset = 31536000; // 1 year
header('Content-Type: text/css; charset=UTF-8');
header('Expires: ' . gmdate( "D, d M Y H:i:s", time() + $expires_offset ) . ' GMT');
header("Cache-Control: public, max-age=$expires_offset");
header('Vary: Accept-Encoding'); // Handle proxies
header('Content-Encoding: gzip');

echo gzencode($out);
exit;