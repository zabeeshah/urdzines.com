<?php $options = get_option('bheem'.'_theme_options');?>
	
    <!-- Footer -->
    <footer class="site-footer">
        <?php if(bheem_set($options, 'hide_newsletter_area')):
			$protocol = is_ssl() ? 'https://' : 'http://';
		?>
        <!-- newsletter part -->
        <div class="p-a 30 bg-primary dez-newsletter">
            <div class="container equal-wraper">
                <div class="row">
                    <form action="<?php echo attr($protocol);?>feedburner.google.com/fb/a/mailverify" accept-charset="utf-8" method="get">
                        <div class="col-md-4 col-sm-4">
                            <div class="icon-bx-wraper equal-col p-t30 p-b20 left">
                                <div class="icon-lg text-primary radius"> <a href="javascript:;" class="icon-cell"><i class="fa fa-envelope-o"></i></a> </div>
                                <div class="icon-content"> <strong class="text-black text-uppercase font-18"><?php echo wp_kses_post(bheem_set($options, 'upper_title'));?></strong>
                                    <h2 class="dez-tilte text-uppercase"><?php echo wp_kses_post(bheem_set($options, 'title'));?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="input-group equal-col p-t40  p-b20">
                                <input type="hidden" id="uri2" name="uri" value="<?php echo esc_attr(bheem_set($options, 'id'));?>">
                                <input name="name" required placeholder="<?php esc_attr_e('Email address', 'bheem'); ?> " class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-sm-offset-1">
                            <div class="input-group equal-col  p-t40 p-b20 skew-subscribe">
                                <button type="button" value="submit" name="submit" class="site-button-secondry button-skew z-index1"> <span><?php esc_html_e('Subscribe', 'bheem'); ?></span><i class="fa fa-angle-right"></i> </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php endif;?>
        
        
		<?php if(!(bheem_set($options, 'hide_upper_footer'))):?>
            <?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
                <!-- footer top part -->
                <div class="footer-top">
                    <div class="container">
                        <div class="row">
                            <?php dynamic_sidebar( 'footer-sidebar' ); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif;?>
        
        
        <?php if(!(bheem_set($options, 'hide_bottom_footer'))):?>
        <!-- footer bottom part -->
        <div class="footer-bottom footer-line">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-left"> <span><?php echo wp_kses_post(bheem_set($options, 'copyright'));?></span> </div>
					<div class="col-md-4 text-center"> <span> <?php echo wp_kses_post(bheem_set($options, 'author_by'));?> </span> </div>
					<div class="col-md-4 text-right "> 
                    	<ul>
							<?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'container_id' => 'navbar-collapse-1',
                                'container_class'=>'navbar-collapse collapse navbar-right',
                                'menu_class'=>'nav navbar-nav',
                                'fallback_cb'=>false, 
                                'items_wrap' => '%3$s', 
                                'container'=>false,
								'depth' => 1,
                                'walker'=> '' 
                            ) ); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>
    </footer>
    <!-- Footer END-->
    
    <!-- scroll top button -->
    <button class="scroltop fa fa-arrow-up style5" ></button>
	
<!-- End Page Wrapper -->

<?php wp_footer(); ?>
</body>
</html>