<?php $options = _WSH()->option();
	get_header(); 
	$settings  = bheem_set(bheem_set(get_post_meta(get_the_ID(), 'bunch_page_meta', true) , 'bunch_page_options') , 0);
	$meta = _WSH()->get_meta('_bunch_layout_settings');
	$meta1 = _WSH()->get_meta('_bunch_header_settings');
	$meta2 = _WSH()->get_meta();
	_WSH()->page_settings = $meta;
	if(bheem_set($_GET, 'layout_style')) $layout = bheem_set($_GET, 'layout_style'); else
	$layout = bheem_set( $meta, 'layout', 'full' );
	if( !$layout || $layout == 'full' || bheem_set($_GET, 'layout_style')=='full' ) $sidebar = ''; else
	$sidebar = bheem_set( $meta, 'sidebar', 'default-sidebar' );
	
	$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
	$layout = ( $layout ) ? $layout : 'right';
	
	$classes = ( !$layout || $layout == 'full' || bheem_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-md-9 ' ;
	/** Update the post views counter */
	_WSH()->post_views( true );
	$bg = bheem_set($meta1, 'header_img');
	$title = bheem_set($meta1, 'header_title');
	$text = bheem_set($meta1, 'header_text');
?>

<!-- inner page banner -->
<div class="dez-bnr-inr overlay-black-middle" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="dez-bnr-inr-entry">
            <h1 class="text-white"><?php if($title) echo wp_kses_post($title); else wp_title('');?></h1>
            <?php if($text):?>
                <div class="dez-separator bg-primary"></div>
                <p class="text-white max-w800"><?php echo wp_kses_post($text); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- inner page banner END -->
<!-- Breadcrumb row -->
<div class="breadcrumb-row">
    <div class="container">
        <?php echo wp_kses_post(bheem_get_the_breadcrumb()); ?>
    </div>
</div>
<!-- Breadcrumb row END -->

<!--Sidebar Page-->
<div class="page-content default-blog-single">
    <div class="content-area">
        <div class="container">
            <div class="row clearfix">
            
            <!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
				<?php if ( is_active_sidebar( $sidebar ) ) { ?>
					<div class="col-md-3">
                        <aside  class="side-bar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    </div>
				<?php } ?>
			<?php endif; ?>
            
            <!--Content Side-->	
            <div class="<?php echo esc_attr($classes);?>">
                <div class="thm-unit-test">
                <!--Default Section-->
                <?php while( have_posts() ): the_post(); 
					$post_meta = _WSH()->get_meta();
				?>
                    
                    <!-- blog start -->
                    <div class="blog-post blog-single">
                        <div class="dez-post-meta m-b20">
                            <ul>
                                <li class="post-date"> <i class="fa fa-calendar"></i><strong><?php echo get_the_date(); ?></strong></li>
                                <li class="post-author"><i class="fa fa-user"></i><?php esc_html_e('By', 'bheem'); ?> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a> </li>
                                <li class="post-comment"><i class="fa fa-comments"></i> <a href="<?php echo esc_url(get_the_permalink(get_the_id()).'#comment');?>"><?php comments_number( wp_kses_post(__('0 Comments' , 'bheem')), wp_kses_post(__('1 Comment' , 'bheem')), wp_kses_post(__('% Comments' , 'bheem'))); ?></a> </li>
                            </ul>
                        </div>
                        <?php if(has_post_thumbnail()):?>
                        <div class="dez-post-media"> <?php the_post_thumbnail('bheem_1170x600'); ?> </div>
                        <?php endif;?>
                        <div class="dez-post-text text clearfix">
                            <?php the_content();?>
                            <div class="clearfix"></div>
                            <?php wp_link_pages(array('before'=>'<div class="paginate-links">'.esc_html__('Pages: ', 'bheem'), 'after' => '</div>', 'link_before'=>'<span>', 'link_after'=>'</span>')); ?>
                        </div>
                        <?php the_tags('<div class="dez-post-tags clear"><div class="post-tags">', '', '</div></div>');?>
                    </div>
					
                    <!-- comment area -->
					<?php comments_template(); ?><!-- end comments -->    
                    
                    <!--Posts Nav-->
                    <div class="posts-nav">
                        <div class="clearfix">
                            <div class="pull-left">
                                <?php previous_post_link('%link','<div class="prev-post"><span class="fa fa-long-arrow-left"></span> &nbsp;&nbsp;&nbsp; Prev Post</div>'); ?>
                            </div>
                            <div class="pull-right">
                                <?php next_post_link('%link','<div class="next-post">Next Post &nbsp;&nbsp;&nbsp; <span class="fa fa-long-arrow-right"></span> </div>'); ?>
                            </div>                                
                        </div>
                    </div>
                    
            	<?php endwhile;?>
                </div>
            </div>
            <!--Content Side-->
            
            <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>
				<?php if ( is_active_sidebar( $sidebar ) ) { ?>
					<div class="col-md-3">
                        <aside  class="side-bar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    </div>
				<?php } ?>
			<?php endif; ?>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>