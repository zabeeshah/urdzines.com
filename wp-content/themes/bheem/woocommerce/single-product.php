<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$options = _WSH()->option();
$meta = _WSH()->get_meta('_bunch_layout_settings');
$meta1 = _WSH()->get_meta('_bunch_header_settings');

$layout = bheem_set( $meta, 'layout');
$sidebar = bheem_set( $meta, 'sidebar');

$layout = ($layout) ? $layout : bheem_set($options, 'woocommerce_single_page_layout');
$sidebar = ($sidebar) ? $sidebar : bheem_set($options, 'woocommerce_single_page_sidebar');

$classes = ( !$layout || $layout == 'full' || bheem_set($_GET, 'layout_style')=='full' ) ? 'col-lg-12 col-md-12 col-sm-12 col-xs-12' : 'col-lg-9 col-md-8 col-sm-12 col-xs-12';

$bg = bheem_set($meta1, 'header_img');
$title = bheem_set($meta1, 'header_title');
$text = bheem_set($meta1, 'header_text');

$bg = ($bg) ? $bg : bheem_set($options, 'woocommerce_page_header_img');
$title = ($title) ? $title : bheem_set($options, 'woocommerce_page_header_title');
$text = ($text) ? $text : bheem_set($options, 'woocommerce_page_header_text');

get_header( 'shop' ); ?>

<!-- inner page banner -->
<div class="dez-bnr-inr overlay-black-middle" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="dez-bnr-inr-entry">
            <h1 class="text-white"><?php if($title) echo wp_kses_post($title); else wp_title('');?></h1>
            <?php if($text):?>
                <div class="dez-separator bg-primary"></div>
                <p class="text-white max-w800"><?php echo wp_kses_post($text); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- inner page banner END -->

<!-- Breadcrumb row -->
<div class="breadcrumb-row">
    <div class="container">
        <?php echo wp_kses_post(bheem_get_the_breadcrumb()); ?>
    </div>
</div>
<!-- Breadcrumb row END -->

<div class="page-content  bg-white">
    <div class="content-area">
        <!-- Product details -->
        <div class="container woo-entry">
            <div class="row m-b30">
    
                
                <!-- sidebar area -->
                <?php if( $layout == 'left' ): ?>
                    <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                        <div class="col-md-3 col-sm-4">  
                            <?php dynamic_sidebar( $sidebar ); ?>
                            <?php
                                /**
                                 * woocommerce_sidebar hook
                                 *
                                 * @hooked woocommerce_get_sidebar - 10
                                 */
                                do_action( 'woocommerce_sidebar' );
                            ?>
                        </div>
                    <?php } ?>
                <?php endif; ?>
                
                <div class="<?php echo esc_attr($classes);?>">
                <?php
                    /**
                     * woocommerce_before_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                     * @hooked woocommerce_breadcrumb - 20
                     */
                    do_action( 'woocommerce_before_main_content' );
                ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php wc_get_template_part( 'content', 'single-product' ); ?>
                    <?php endwhile; // end of the loop. ?>
                <?php
                    /**
                     * woocommerce_after_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
                    do_action( 'woocommerce_after_main_content' );
                ?>
                
                </div>
            
                <!-- sidebar area -->
                <?php if( $layout == 'right' ): ?>
                    <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                        <div class="col-md-3 col-sm-4">  
                            <?php dynamic_sidebar( $sidebar ); ?>
                            <?php
                                /**
                                 * woocommerce_sidebar hook
                                 *
                                 * @hooked woocommerce_get_sidebar - 10
                                 */
                                do_action( 'woocommerce_sidebar' );
                            ?>
                        </div>
                    <?php } ?>
                <?php endif; ?>
            
            </div>
        </div>
    </div>
</div>
<?php get_footer( 'shop' ); ?>
