<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$options = _WSH()->option();
$meta = _WSH()->get_meta('_bunch_layout_settings', get_option( 'woocommerce_shop_page_id' ));
$meta1 = _WSH()->get_meta('_bunch_header_settings', get_option( 'woocommerce_shop_page_id' ));

$layout = bheem_set( $meta, 'layout');
$layout = bheem_set( $_GET, 'layout' ) ? $_GET['layout'] : $layout; 
if(bheem_set($_GET, 'layout_style')) $layout = bheem_set($_GET, 'layout_style'); else

$layout = bheem_set( $meta, 'layout');
$sidebar = bheem_set( $meta, 'sidebar');

$layout = ($layout) ? $layout : bheem_set($options, 'woo_cat_page_layout');
$sidebar = ($sidebar) ? $sidebar : bheem_set($options, 'woocommerce_cat_page_sidebar');

if( !$layout || $layout == 'full' || bheem_set($_GET, 'layout_style')=='full' ) $classes[] = 'col-md-3 col-sm-6 m-b30 product-item card-container'; else $classes[] = 'col-md-4 col-sm-6 m-b30 product-item card-container'; 
//$pro_num = bheem_set($options, 'number_of_product_column');
//if($pro_num == '2') $classes[] = '';
//elseif($pro_num == '4') $classes[] = '';
//else $classes[] = '';
?>
<div <?php post_class( $classes ); ?>>
	<div class="dez-box">
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	 ?>
     
     <?php 
		$post_thumbnail_id = get_post_thumbnail_id($post->ID);
		$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
	 ?>
     <div class="dez-thum-bx dez-img-effect mfp-gallery zoom-slow"> <?php woocommerce_template_loop_product_thumbnail();?>
        <div class="overlay-bx">
            <div class="overlay-icon"> 
            	<a href="<?php echo esc_url(get_the_permalink(get_the_id()));?>"> <i class="fa fa-cart-plus icon-bx-xs"></i> </a> 
                <a href="<?php echo esc_url($post_thumbnail_url);?>" class="mfp-link"> <i class="fa fa-search icon-bx-xs"></i> </a> 
                
				<?php $like = get_post_meta( get_the_id(), '_jolly_like_it', true ); ?>
                <a href="javascript:void(0)" class="jolly_like_it" data-id="<?php the_ID(); ?>"> <i class="fa fa-heart icon-bx-xs"></i> </a> 
            </div>
        </div>
    </div>
     
     <?php do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );
	?>
        
        <div class="dez-info p-a20 text-center">
            <h4 class="dez-title m-t0 text-uppercase"><a href="<?php echo esc_url(get_the_permalink(get_the_id()));?>"><?php the_title();?></a></h4>
            <h2 class="m-b0"><?php woocommerce_template_loop_price();?> </h2>
            <div class="m-t20">
                <a href="<?php echo esc_url(get_the_permalink(get_the_id()));?>" class="site-button"><?php esc_html_e('Add To Cart', 'bheem'); ?>	</a>
            </div>
        </div>
        
	<?php /**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>
	</div>
</div>
