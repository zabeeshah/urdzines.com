<!--Blog Post-->
<div class="blog-post blog-lg date-style-2">
    <?php if(has_post_thumbnail()):?>
    <div class="dez-post-media dez-img-effect zoom-slow"> <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_post_thumbnail('bheem_1170x600'); ?></a> </div>
    <?php endif; ?>
    <div class="dez-post-info">
        <div class="dez-post-title ">
            <h3 class="post-title"><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
        </div>
        <div class="dez-post-meta ">
            <ul>
                <?php if(has_post_thumbnail()):?>
                <li class="post-date"> <i class="fa fa-calendar"></i><strong><?php echo get_the_date(); ?></strong></li>
                <?php endif; ?>
                <li class="post-author"><i class="fa fa-user"></i><?php esc_html_e('By', 'bheem'); ?> <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a> </li>
                <li class="post-comment"><i class="fa fa-comments"></i> <a href="<?php echo esc_url(get_the_permalink(get_the_id()).'#comment');?>"><?php comments_number( wp_kses_post(__('0 Comments' , 'bheem')), wp_kses_post(__('1 Comment' , 'bheem')), wp_kses_post(__('% Comments' , 'bheem'))); ?></a> </li>
                <?php the_tags('<li class="post-tag">  <i class="fa fa-tag"></i> ', ', ', '</li>');?></li>
            </ul>
        </div>
        <div class="dez-post-text f-15">
            <?php the_excerpt();?>
        </div>
        <div class="dez-post-readmore"> <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>" title="<?php esc_attr_e('READ MORE', 'bheem'); ?>" rel="bookmark" class="site-button-link"><?php esc_html_e('Read More', 'bheem'); ?></a> </div>
    </div>
</div>