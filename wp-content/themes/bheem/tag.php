<?php bheem_bunch_global_variable();
	$options = _WSH()->option();
	get_header(); 
	$meta = _WSH()->get_term_meta( '_bunch_category_settings' );
	if(bheem_set($_GET, 'layout_style')) $layout = bheem_set($_GET, 'layout_style'); else
	$layout = bheem_set( $meta, 'layout', 'right' );
	$sidebar = bheem_set( $meta, 'sidebar', 'default-sidebar' );
	$view = bheem_set( $meta, 'view', 'list' ) ? bheem_set( $meta, 'view', 'list' ) : 'list';
	_WSH()->page_settings = array('layout'=>$layout, 'sidebar'=>$sidebar);
	
	$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
	$layout = ( $layout ) ? $layout : 'right';
	
	$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12' : ' col-md-9 ';
	$bg = bheem_set($meta, 'header_img');
	$title = bheem_set($meta, 'header_title');
	$text = bheem_set($meta, 'header_text');
?>

<!-- inner page banner -->
<div class="dez-bnr-inr overlay-black-middle" <?php if($bg):?>style="background-image:url('<?php echo esc_url($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="dez-bnr-inr-entry">
            <h1 class="text-white"><?php if($title) echo wp_kses_post($title); else wp_title('');?></h1>
            <?php if($text):?>
                <div class="dez-separator bg-primary"></div>
                <p class="text-white max-w800"><?php echo wp_kses_post($text); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- inner page banner END -->
<!-- Breadcrumb row -->
<div class="breadcrumb-row">
    <div class="container">
        <?php echo wp_kses_post(bheem_get_the_breadcrumb()); ?>
    </div>
</div>
<!-- Breadcrumb row END -->

<div class="page-content">
    <!--Sidebar Page-->
    <div class="content-area">
        <div class="container">
            <div class="row clearfix">
            
                <!-- sidebar area -->
                <?php if( $layout == 'left' ): ?>
                    <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                        <div class="col-md-3">
                            <aside  class="side-bar">
                                <?php dynamic_sidebar( $sidebar ); ?>
                            </aside>
                        </div>
                    <?php } ?>
                <?php endif; ?>
                
                <!-- Left Content -->
                <div class="<?php echo esc_attr($classes);?>">
                    <div class="thm-unit-test">
                    <?php while( have_posts() ): the_post();?>
                        <!-- blog post item -->
                        <!-- Post -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class();?>>
                            <?php get_template_part( 'blog' ); ?>
                        <!-- blog post item -->
                        </div><!-- End Post -->
                    <?php endwhile;?> 
                    </div>
                    <!--Pagination-->
                    <div class="pagination-bx clearfix">
                        <?php bheem_the_pagination(); ?>
                    </div>
                
                </div>
                <!-- sidebar area -->
                
                <!-- sidebar area -->
                <?php if( $layout == 'right' ): ?>
                    <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                        <div class="col-md-3">
                            <aside  class="side-bar">
                                <?php dynamic_sidebar( $sidebar ); ?>
                            </aside>
                        </div>
                    <?php }?>
                <?php endif; ?>
                <!-- sidebar area -->
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>