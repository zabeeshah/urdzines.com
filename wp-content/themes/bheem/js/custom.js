(function($) {
	
	"use strict";

//------like end-----
	 var bunch_theme = {   
	   count: 0,
	   likeit: function(options, selector)
	   {
		options.action = '_bunch_ajax_callback';
		
		if( $(selector).data('_bunch_like_it') === true ){
		 bunch_theme.msg( 'You have already done this job', 'error' );
		 return;
		}
		
		$(selector).data('_bunch_like_it', true );
	
		bunch_theme.loading(true);
		
		$.ajax({
		 url: ajaxurl,
		 type: 'POST',
		 data:options,
		 dataType:"json",
		 success: function(res){
	
		  try{
		   var newjason = res;
	
		   if( newjason.code === 'fail'){
			$(selector).data('_bunch_like_it', false );
			bunch_theme.loading(false);
			bunch_theme.msg( newjason.msg, 'error' );
		   }else if( newjason.code === 'success' ){
			bunch_theme.loading(false);
			$(selector).data('_bunch_like_it', true );
			bunch_theme.msg( newjason.msg, 'success' );
		   }
		   
		  }
		  catch(e){
		   bunch_theme.loading(false);
		   $(selector).data('_bunch_like_it', false );
		   bunch_theme.msg( 'There was an error with request '+e.message, 'error' );
		   
		  }
		 }
		});
	   },
	   loading: function( show ){
		if( $('.ajax-loading' ).length === 0 ) {
		 $('body').append('<div class="ajax-loading" style="display:none;"></div>');
		}
		
		if( show === true ){
		 $('.ajax-loading').show('slow');
		}
		if( show === false ){
		 $('.ajax-loading').hide('slow');
		}
	   },
	   
	   msg: function( msg, type ){
		if( $('#pop' ).length === 0 ) {
		 $('body').append('<div style="display: none;" id="pop"><div class="pop"><div class="alert"><p></p></div></div></div>');
		}
		if( type === 'error' ) {
		 type = 'danger';
		}
		var alert_type = 'alert-' + type;
		
		$('#pop > .pop p').html( msg );
		$('#pop > .pop > .alert').addClass(alert_type);
		
		$('#pop').slideDown('slow').delay(5000).fadeOut('slow', function(){
		 $('#pop .pop .alert').removeClass(alert_type);
		});
		
		
	   },
	   
	  };
  
 //------like end-----



/**
Core script to handle the entire theme and core functions
**/
var Bheem = function(){
	
	var cart_on_mobile = bheem_data.cart_on_mobile;
	
	/* Search Bar ============ */
	var homeSearch = function() {
		'use strict';
		/* top search in header on click function */
		var quikSearch = jQuery("#quik-search-btn");
		var quikSearchRemove = jQuery("#quik-search-remove");
		
		quikSearch.on('click',function() {
			jQuery('.dez-quik-search').animate({'width': '100%' });
			jQuery('.dez-quik-search').delay(500).css({'left': '0'  });
		});
		
		quikSearchRemove.on('click',function() {
			jQuery('.dez-quik-search').animate({'width': '0%' ,  'right': '0'  });
			jQuery('.dez-quik-search').css({'left': 'auto'  });
		});	
		/* top search in header on click function End*/
	};
	
	var cartButton = function(){
		$('.cart-btn').on('click',function(e){
			e.preventDefault();
			$(".minicart").slideToggle('slow');
		})
		
		if(jQuery( window ).width() <= 360)
		{
			if(cart_on_mobile == 0)
			{
				jQuery('.shop-cart').hide();
			}
		}
	}
	
	/* One Page Layout ============ */
	var onePageLayout = function() {
		'use strict';
		var headerHeight =   parseInt($('.onepage').css('height'), 10);
		$(".scroll").on('click',function(event) 
		{
			event.preventDefault();
			
			if (this.hash !== "") {
				var hash = this.hash;	
				var seactionPosition = $(hash).offset().top;
				var headerHeight =   parseInt($('.onepage').css('height'), 10);
				//alert(headerHeight);
				
				$('body').scrollspy({target: ".navbar", offset: headerHeight+2}); 
				
				var scrollTopPosition = seactionPosition - (headerHeight);
				
				$('html, body').animate({
					scrollTop: scrollTopPosition
				}, 800, function(){
					//window.location.hash = hash;
				});
			}   
		});
		$('body').scrollspy({target: ".navbar", offset: headerHeight + 2});  
	};
	
	/* Load File ============ */
	var dzTheme = function(){
		 'use strict';
		 var loadingImage = '<img src="images/loading.gif">';
		 jQuery('.dzload').each(function(){
		 var dzsrc = $(this).attr('dzsrc');
		  //jQuery(this).html(loadingImage);
			jQuery(this).hide(function(){
			   jQuery(this).load(dzsrc, function(){
					jQuery(this).fadeIn('slow');
			   }); 
			});
		 });
	};
	
	/* Magnific Popup ============ */
	var MagnificPopup = function(){
		'use strict';	
		/* magnificPopup function */
		jQuery('.mfp-gallery').magnificPopup({
			delegate: '.mfp-link',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
				}
			}
		});
		/* magnificPopup function end */
		
		/* magnificPopup for paly video function */		
		jQuery('.video').magnificPopup({
			type: 'iframe',
			iframe: {
				markup: '<div class="mfp-iframe-scaler">'+
						 '<div class="mfp-close"></div>'+
						 '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
						 '<div class="mfp-title">Some caption</div>'+
						 '</div>'
			},
			callbacks: {
				markupParse: function(template, values, item) {
					values.title = item.el.attr('title');
				}
			}
		});
		/* magnificPopup for paly video function end*/
		
	};
	
	// Magnific Popup With OWL ============ /
	var MagnificPopupWithOwl = function(){
		'use strict';	
		// magnificPopup function /
		jQuery('.mfp-gallery-with-owl').magnificPopup({
			delegate: '.owl-item:not(.cloned) .mfp-link',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') + '<small>by inimexlifting.com</small>';
				}
			}
		});
		// magnificPopup function end /
		
		
	};
	
	/* Scroll To Top ============ */
	var scrollTop = function (){
		'use strict';
		var scrollTop = jQuery("button.scroltop");
		/* page scroll top on click function */	
		scrollTop.on('click',function() {
			jQuery("html, body").animate({
				scrollTop: 0
			}, 1000);
			return false;
		});

		jQuery(window).bind("scroll", function() {
			var scroll = jQuery(window).scrollTop();
			if (scroll > 900) {
				jQuery("button.scroltop").fadeIn(1000);
			} else {
				jQuery("button.scroltop").fadeOut(1000);
			}
		});
		/* page scroll top on click function end*/
	};
	
	/* Handel Accordian ============ */
	var handelAccordian = function(){
		/* accodin open close icon change */	 	
		jQuery('#accordion').on('hidden.bs.collapse', function(e){
			jQuery(e.target)
				.prev('.panel-heading')
				.find("i.indicator")
				.toggleClass('glyphicon-minus glyphicon-plus');
		});
		jQuery('#accordion').on('shown.bs.collapse', function(e){
			jQuery(e.target)
				.prev('.panel-heading')
				.find("i.indicator")
				.toggleClass('glyphicon-minus glyphicon-plus');
		});
		/* accodin open close icon change end */
	};
	
	/* Handel Placeholder ============ */
	var handelPlaceholder = function(){
		/* input placeholder for ie9 & ie8 & ie7 */
		jQuery.support.placeholder = ('placeholder' in document.createElement('input'));
		/* input placeholder for ie9 & ie8 & ie7 end*/
		
		/*fix for IE7 and IE8  */
		if (!jQuery.support.placeholder) {
			jQuery("[placeholder]").focus(function () {
				if (jQuery(this).val() == jQuery(this).attr("placeholder")) jQuery(this).val("");
			}).blur(function () {
				if (jQuery(this).val() == "") jQuery(this).val(jQuery(this).attr("placeholder"));
			}).blur();

			jQuery("[placeholder]").parents("form").submit(function () {
				jQuery(this).find('[placeholder]').each(function() {
					if (jQuery(this).val() == jQuery(this).attr("placeholder")) {
						 jQuery(this).val("");
					}
				});
			});
		}
		/*fix for IE7 and IE8 end */
	};
	
	/* File Input ============ */
	var fileInput = function(){
		'use strict';
		/* Input type file jQuery */	 	 
		jQuery(document).on('change', '.btn-file :file', function() {
			var input = jQuery(this);
			var	numFiles = input.get(0).files ? input.get(0).files.length : 1;
			var	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});
		
		jQuery('.btn-file :file').on('fileselect', function(event, numFiles, label) {
			input = jQuery(this).parents('.input-group').find(':text');
			var log = numFiles > 10 ? numFiles + ' files selected' : label;
		
			if (input.length) {
				input.val(log);
			} else {
				if (log) alert(log);
			}
		});
		/* Input type file jQuery end*/
	};
	
	/* Header Fixed ============ */
	var headerFix = function(){
		'use strict';
		/* Main navigation fixed on top  when scroll down function custom */		
		jQuery(window).bind('scroll', function () {
			var menu = jQuery('.sticky-header');
			if ($(window).scrollTop() > menu.offset().top) {
				menu.addClass('is-fixed');
			} else {
				menu.removeClass('is-fixed');
			}
		});
		/* Main navigation fixed on top  when scroll down function custom end*/
	};
	
	/* Masonry Box ============ */
	var masonryBox = function(){
		'use strict';
		/* masonry by  = bootstrap-select.min.js */ 
		var self = $("#masonry");
		self.imagesLoaded(function () {
			self.masonry({
				gutterWidth: 15,
				isAnimated: true,
				itemSelector: ".card-container"
			});
		});

		jQuery(".filters").on('click','li',function(e) {
			e.preventDefault();
			var filter = $(this).attr("data-filter");
			self.masonryFilter({
				filter: function () {
					if (!filter) return true;
					return $(this).attr("data-filter") == filter;
				}
			});
		});
		/* masonry by  = bootstrap-select.min.js end */
	};
	
	/* Set Div Height ============ */
	var setDivHeight = function(){	
		'use strict';
		var allHeights = [];
		jQuery('.dzseth > div, .dzseth .img-cover').each(function(e){
			allHeights.push(jQuery(this).height());
		});

		jQuery('.dzseth > div, .dzseth .img-cover').each(function(e){
			var maxHeight = Math.max.apply(Math,allHeights);
			jQuery(this).css('height',maxHeight);
		});
		
		allHeights = [];
		/* Removice */
		var screenWidth = $( window ).width();
		if(screenWidth < 991)
		{
			jQuery('.dzseth > div, .dzseth .img-cover').each(function(e){
				jQuery(this).css('height','');
			});
		}	
	};
	
	/* Counter Number ============ */
	var counter = function(){
		jQuery('.counter').counterUp({
			delay: 10,
			time: 3000
		});
	};
	
	/* Video Popup ============ */
	var handelVideo = function(){
		/* Video responsive function */	
		jQuery('iframe[src*="youtube.com"]').wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
		jQuery('iframe[src*="vimeo.com"]').wrap('<div class="embed-responsive embed-responsive-16by9"></div>');	
		/* Video responsive function end */
	};
	
	/* Gallery Filter ============ */
	var handelFilterMasonary = function(){
		/* gallery filter activation = jquery.mixitup.min.js */ 
		if (jQuery('#image-gallery-mix').length) {
			jQuery('.gallery-filter').find('li').each(function () {
				$(this).addClass('filter');
			});
			jQuery('#image-gallery-mix').mixItUp();
		}
		if(jQuery('.gallery-filter.masonary').length){
			jQuery('.gallery-filter.masonary').on('click','span', function(){
				var selector = $(this).parent().attr('data-filter');
				jQuery('.gallery-filter.masonary span').parent().removeClass('active');
				jQuery(this).parent().addClass('active');
				jQuery('#image-gallery-isotope').isotope({ filter: selector });
				return false;
			});
		}
		/* gallery filter activation = jquery.mixitup.min.js */
	};
	
	/* Handel Bootstrap Select ============ */
	var handelBootstrapSelect = function(){
		/* Bootstrap Select box function by  = bootstrap-select.min.js */ 
		jQuery('select').selectpicker();
		/* Bootstrap Select box function by  = bootstrap-select.min.js end*/
	};
	
	/* Handel Bootstrap Touch Spin ============ */
	var handelBootstrapTouchSpin = function(){
		jQuery("input[name='demo_vertical2']").TouchSpin({
		  verticalbuttons: true,
		  verticalupclass: 'glyphicon glyphicon-plus',
		  verticaldownclass: 'glyphicon glyphicon-minus'
		});
		
	};
	
	/* Countdown ============ */
	var handelCountDown = function(){
		/* Time Countr Down Js */
		if($(".countdown").length)
		{
			$('.countdown').countdown({date: '30 march 2018 23:5'}, function() {
				$('.countdown').text('we are live');
			});
		}
		/* Time Countr Down Js End */
	};
	
	/* Content Scroll ============ */
	var handelCustomScroll = function(){
		/* all available option parameters with their default values */
		if($(".content-scroll").length)
		{ 
			$(".content-scroll").mCustomScrollbar({
				setWidth:false,
				setHeight:false,
				axis:"y"
			});	
		}
	};
	
	
	/* Left Menu ============ */
	var handelSideBarMenu = function(){
		$('.openbtn').on('click',function(e){
			e.preventDefault();
			if($('#mySidenav').length > 0)
			{
				document.getElementById("mySidenav").style.left = "0";
			}

			if($('#mySidenav1').length > 0)
			{
				document.getElementById("mySidenav1").style.right = "0";
			}
			
		});
		
		$('.closebtn').on('click',function(e){
			e.preventDefault();
			if($('#mySidenav').length > 0)
			{
				document.getElementById("mySidenav").style.left = "-300px";
			}
			
			if($('#mySidenav1').length > 0)
			{
				document.getElementById("mySidenav1").style.right = "-820px";
			}
		});
	}
	
	/* Function ============ */
	return {
		init:function(){
			onePageLayout();
			dzTheme();
			homeSearch();
			MagnificPopup();
			MagnificPopupWithOwl();
			handelAccordian();
			scrollTop();
			handelPlaceholder();
			fileInput();
			headerFix();
			cartButton();
			setDivHeight();
			counter();
			handelVideo();
			handelFilterMasonary();
			handelCountDown();
			handelCustomScroll();
			handelSideBarMenu();
		},
		
		load:function(){
			masonryBox();
			handelBootstrapSelect();
			handelBootstrapTouchSpin();
		}
		
	}
	
}();


/* Document.ready Start */	
jQuery(document).ready(function() {
    'use strict';
	Bheem.init();
	
	var screenWidth = jQuery( window ).width();
	if(screenWidth <= 991 )
	{
		jQuery('.site-header .navbar-nav a > i').on('click', function(e){
			e.preventDefault();
			if(jQuery(this).parent().parent().hasClass('open'))
			{
				jQuery(this).parent().parent().removeClass('open');
			}
			else{
				jQuery(this).parent().parent().parent().find('li').removeClass('open');
				jQuery(this).parent().parent().addClass('open');
			}
		});
	}
});
/* Document.ready END */

/* Window Load START */
jQuery(window).load(function () {
	'use strict'; 
	Bheem.load();
	
	setTimeout(function(){
		jQuery('#loading-area').remove();
	}, 0);
	
});
/*  Window Load END */

$('.jolly_like_it').on('click', function(e) {
		   
	 e.preventDefault();
	 
	 var opt = {subaction:'likeit', data_id:$(this).attr('data-id')};
	 bunch_theme.likeit( opt, this );
	 
	 setTimeout(function(){
	   window.location.reload(1);
	}, 1500);
	
	 return false;
});/**like end*/


})(window.jQuery);